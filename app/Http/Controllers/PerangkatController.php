<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Lokasi;
use App\Models\Tipe;
use App\Models\Syslog;
use App\Models\Perangkat;
use Alert;
use Auth;

class PerangkatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->title = 'Perangkat';
    }

    public function index()
    {
        $title = $this->title;
        $perangkat = Perangkat::all();
        return view('perangkat',compact('title','perangkat'));
    }

    public function detail($id)
    {
        $title = $this->title;
        $perangkat = Perangkat::find($id);
        $syslog = Syslog::where('FromHost',$perangkat->host)->orderByDesc('ReceivedAt')->get();

        return view('perangkat-detail',compact('title','perangkat','syslog'));

    }

    public function tambah()
    {
        if(Auth::user()->role_id!= 2){
            return redirect()->route('perangkat');
        }
        //echo 'hahaha';
        $title = $this->title;
        $brand = Brand::all('id','brand');
        $tipe = Tipe::all('id','tipe');
        $lokasi = Lokasi::all('id','lokasi');
        $host = Syslog::groupBy('FromHost')->get('FromHost');

        $data = [
            $brand,$tipe,$lokasi,$host
        ];

        //dd($data);
        return view('tambah-perangkat',compact('title','brand','lokasi','tipe','host'));
    }

    public function simpan(Request $request)
    {
        if(Auth::user()->role_id!= 2){
            return redirect()->route('perangkat');
        }
        $request->validate([
            'nama_perangkat'    => 'required',
            'serial_number'     => 'required',
            'brand_id'          => 'required',
            'lokasi_id'         => 'required',
            'tipe_id'           => 'required',
            'deskripsi'         => 'required',
            'host'              => 'required'
        ],[
            'nama_perangkat.required'   => 'Nama perangkat wajib di isi',
            'serial_number.required'    => 'Serial number wajib di isi',
            'brand_id.required'         => 'Brand wajib dipilih',
            'lokasi_id.required'        => 'Lokasi wajib dipilih',
            'tipe_id.required'          => 'Tipe wajib dipilih',
            'dekripsi.required'         => 'Deskripsi wajib di isi',
            'host.required'             => 'Host wajib di isi'
        ]);
        //dd($request);

        Perangkat::create($request->all());

        Alert::success('Berhasil','Berhasil menambahkan perangkat');
        return redirect()->route('perangkat');

    }

    public function edit($id)
    {
        if(Auth::user()->role_id!= 2){
            return redirect()->route('perangkat');
        }
        $title = $this->title;
        $perangkat = Perangkat::find($id);
        $brand = Brand::all('id','brand');
        $tipe = Tipe::all('id','tipe');
        $lokasi = Lokasi::all('id','lokasi');
        $host = Syslog::groupBy('FromHost')->get('FromHost');

        return view('edit-perangkat', compact('title','perangkat','brand','tipe','lokasi','host'));

    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role_id!= 2){
            return redirect()->route('perangkat');
        }
        $request->validate([
            'nama_perangkat'    => 'required',
            'serial_number'     => 'required',
            'brand_id'          => 'required',
            'lokasi_id'         => 'required',
            'tipe_id'           => 'required',
            'deskripsi'         => 'required',
            'host'              => 'required'
        ],[
            'nama_perangkat.required'   => 'Nama perangkat wajib di isi',
            'serial_number.required'    => 'Serial number wajib di isi',
            'brand_id.required'         => 'Brand wajib dipilih',
            'lokasi_id.required'        => 'Lokasi wajib dipilih',
            'tipe_id.required'          => 'Tipe wajib dipilih',
            'dekripsi.required'         => 'Deskripsi wajib di isi',
            'host.required'             => 'Host wajib di isi'
        ]);

        $perangkat = Perangkat::find($id);
        $perangkat->update($request->all());

        Alert::success('Berhasil','Perangkat berhasil di update');
        return redirect()->route('perangkat');
    }

    public function delete(Request $request)
    {
        if(Auth::user()->role_id!= 2){
            return redirect()->route('perangkat');
        }
        $perangkat = Perangkat::find($request->id);
        $perangkat->delete();
        Alert::success('Berhasil','Hapus perangkat berhasil');

        return redirect()->route('perangkat');
    }
}
