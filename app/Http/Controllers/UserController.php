<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Alert;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->title = 'Kelola User';
    }

    public function index()
    {
        $title = $this->title;
        $users = User::all();
        return view('kelola-user',compact('title','users'));
    }

    public function detail()
    {
        # code...
    }

    public function tambah()
    {
        $title  = $this->title;
        $roles  = Role::all();
        return view('tambah-user',compact('title','roles'));
    }

    public function simpan(Request $request)
    {
        $request->validate([
            'email'         => 'required',
            'name'          => 'required',
            'role_id'       => 'required',
            'password'      => 'required|confirmed'

        ]);

        $data = [
            'email'     => $request->email,
            'name'      => $request->name,
            'role_id'   => $request->role_id,
            'password'  => Hash::make($request->password)
        ];

        User::create($data);

        Alert::success('Sukses','Berhasil menambahkan user');
        return redirect()->route('user');
    }

    public function edit(Request $request, $id)
    {
        $title  = $this->title;
        $user   = User::find($id);
        $roles  = Role::all();

        return view('edit-user',compact('title','user','roles'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'email'         => 'required',
            'name'          => 'required',
            'role_id'       => 'required',
        ]);

        $user = User::find($request->id);

        $data = [
            'email'     => $request->email,
            'name'      => $request->name,
            'role_id'   => $request->role_id
        ];

        if(isset($request->password)){
            $request->validate([
                'password'  => 'confirmed'
            ]);
            $data['password'] = Hash::make($request->password);
        }

        $user->update($data);

        Alert::success('Sukses','Berhasil update user');
        return redirect()->route('user');
    }

}
