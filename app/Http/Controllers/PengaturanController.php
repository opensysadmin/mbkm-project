<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Lokasi;
use App\Models\Tipe;
use Auth;
use Alert;

class PengaturanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->title = 'Pengaturan';
    }
    public function index()
    {
        $title = $this->title;
        $brand = Brand::all();
        $tipe = Tipe::all();
        $lokasi = Lokasi::all();
        return view('pengaturan',compact('title','brand','tipe','lokasi'));
    }

    public function simpan(Request $request)
    {
        $request->validate([
            'parameter' => 'required'
        ]);

        if($request->parameter == 'lokasi'){
            $request->validate([
                'lokasi'    => 'required',
                'alamat'    => 'required'
            ]);

            Lokasi::create($request->all());
            Alert::success('Berhasil','Berhasil menambahkan lokasi');
        }
        elseif($request->parameter  == 'tipe'){
            $request->validate([
                'tipe'  => 'required'
            ]);

            Tipe::create($request->all());
            Alert::success('Berhasil','Berhasil menambahkan tipe');
        }
        elseif($request->parameter == 'brand'){
            $request->validate([
                'brand'  => 'required'
            ]);

            Brand::create($request->all());
            Alert::success('Berhasil','Berhasil menambahkan brand');
        }
        else{

        }

        return redirect()->route('pengaturan');
    }

    public function delete(Request $request)
    {
        $request->validate([
            'parameter' => 'required',
            'id_hapus'  => 'required'
        ]);


        if($request->parameter == 'lokasi'){
            $lokasi = Lokasi::find($request->id_hapus);
            $lokasi->delete();

            Alert::success('Berhasil','Berhasil hapus lokasi');
        }
        elseif($request->parameter  == 'tipe'){
            $tipe = Tipe::find($request->id_hapus);
            $tipe->delete();

            Alert::success('Berhasil','Berhasil hapus tipe');
        }
        elseif($request->parameter == 'brand'){
            $brand = Brand::find($request->id_hapus);
            $brand->delete();

            Alert::success('Berhasil','Berhasil hapus brand');
        }
        else{

        }

        return redirect()->route('pengaturan');
    }

}
