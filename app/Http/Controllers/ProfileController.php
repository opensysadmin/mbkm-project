<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Alert;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->title = 'Profile';
    }

    public function index()
    {
        $title = $this->title;
        return view('profile',compact('title'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'old_password'  => 'required',
            'password'      => 'required|confirmed'
        ]);
        $hashed = Hash::make($request->old_password);

        if(Hash::check($request->password, Auth::user()->password)){
            return redirect()->route('profile')->with('old_password','Password tidak sesuai');
        }

        $user = User::find(Auth::id());
        //dd($user);
        $user->update([
            'password'  => Hash::make($request->password)
        ]);

        Alert::success('Sukses','Berhasil update password');
        return redirect()->route('profile');
    }
}
