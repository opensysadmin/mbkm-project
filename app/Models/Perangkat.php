<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Perangkat extends Model
{
    use HasFactory;

    public $fillable = [
        'nama_perangkat',
        'serial_number',
        'host',
        'deskripsi',
        'tipe_id',
        'brand_id',
        'lokasi_id',
        'add_by'
    ];

    function tipe()
    {
        return $this->belongsTo(Tipe::class);
    }

    function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    function lokasi()
    {
        return $this->belongsTo(Lokasi::class);
    }

    function add_by()
    {
        return $this->belongsTo(User::class,'add_by');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(
            function($model){
                $model->add_by = Auth::id();
            }
        );
    }
}
