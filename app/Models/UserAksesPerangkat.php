<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;


class UserAksesPerangkat extends Model
{
    use HasFactory;

    public $fillabe = [
        'perangkat_id',
        'user_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(
            function($model){
                $model->user_id = Auth::id();
            }
        );
    }
}
