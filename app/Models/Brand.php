<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Brand extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand',
        'user_id',
    ];

    function user()
    {
        return $this->belongsTo(User::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(
            function($model){
                $model->user_id = Auth::id();
            }
        );
    }


}
