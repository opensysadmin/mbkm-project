<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $role_user = \App\Models\Role::where('role','User')->first();
        $role_admin = \App\Models\Role::where('role','Admin')->first();

        $users = [
            [
                'name'              => 'user',
                'email'             => 'user@ltn.net.id',
                'email_verified_at' => now(),
                'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token'    => Str::random(10),
                'role_id'           => $role_user->id,
            ],
            [
                'name'              => 'admin',
                'email'             => 'admin@ltn.net.id',
                'email_verified_at' => now(),
                'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token'    => Str::random(10),
                'role_id'           => $role_admin->id,
            ],
        ];

        foreach($users as $user){
            \App\Models\User::create($user);
        }

    }
}
