<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerangkatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perangkats', function (Blueprint $table) {
            $table->id();
            $table->string('nama_perangkat');
            $table->string('serial_number');
            $table->string('host');
            $table->text('deskripsi');
            $table->unsignedBigInteger('tipe_id');
            $table->unsignedBigInteger('brand_id');
            $table->unsignedBigInteger('lokasi_id');
            $table->unsignedBigInteger('add_by');
            $table->timestamps();
            $table->foreign('tipe_id')->references('id')->on('tipes');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('lokasi_id')->references('id')->on('brands');
            $table->foreign('add_by')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perangkats');
    }
}
