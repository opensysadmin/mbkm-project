@extends('layouts.main')

@section('container')
  <div class="col bg-light p-3 rounded">
    <form method="POST" action="{{route('profile.update')}}">
        @csrf
        @method('PUT')
      <div class="row">
        <h2>Profile Username</h2>
      </div>
      <div class="row m-3">
        <div class="mb-3 row">
          <label for="oldPassowrd" class="col-sm-3 col-form-label">Password Lama</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="old_password" placeholder="Password Lama" name="old_password">
          </div>
          @if(session('old_password'))
            <div class="text-danger">
                {{session('old_password')}}
            </div>
          @endif
        </div>
        <div class="mb-3 row">
          <label for="newPassword" class="col-sm-3 col-form-label">Password Baru</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="password" placeholder="Password Baru" name="password">
          </div>

        </div>
        <div class="mb-3 row">
          <label for="repeatPassword" class="col-sm-3 col-form-label">Ulangi Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="password_confirmation" placeholder="Ulangi Password Baru" name="password_confirmation">
          </div>
          @error('password')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="row mb-3">
          <div class="col-sm-3 align-self-end">
            <button class="btn btn-success" type="submit">Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
@endsection
