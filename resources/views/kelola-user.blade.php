@extends('layouts.main')

@section('container')
  <div class="col bg-light p-3 rounded">
      <div class="row">
        <div class="col-6">
          <h2>Kelola User</h2>
        </div>
        <div class="col-6 justify-content-end">
          <p align="right"><a href="{{route('user.tambah')}}" class="btn btn-success"><i class="bi bi-plus"></i></a></p>
        </div>
      </div>
      <!--<div class="row mt-1">
        <div class="col col-xs-12 m-1 rounded bg-white">
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Nama Perangkat</label>
            <div class="col-sm-8">
              <label class="col-form-label">Switch MMR</label>
            </div>
          </div>
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Serial Number</label>
            <div class="col-sm-8">
              <label class="col-form-label">87NI3ND9DFNDF</label>
            </div>
          </div>
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Brand</label>
            <div class="col-sm-8">
              <label class="col-form-label">Cisco</label>
            </div>
          </div>
        </div>
        <div class="col col-xs-12 m-1 rounded bg-white">
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Lokasi</label>
            <div class="col-sm-8">
              <label class="col-form-label">Datacenter</label>
            </div>
          </div>
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Tipe</label></label>
            <div class="col-sm-8">
              <label class="col-form-label">Switch</label>
            </div>
          </div>
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Deskripsi</label>
            <div class="col-sm-8">
              <label class="col-form-label">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</label>
            </div>
          </div>
        </div>
      </div>-->

      <div class="row mt-2">
        <table class="table table-striped table-hover table-sm">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Email</th>
              <th scope="col">Nama</th>
              <th scope="col">Role</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($users as $row)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$row->email}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->role->role}}</td>
                <td>
                  <a href="{{route('user.edit',['id'=>$row->id])}}" class="btn btn-warning"><i class="bi bi-pencil"></i></a>
                  <a href="#" class="btn btn-danger"><i class="bi bi-trash3-fill"></i></a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>


  </div>
@endsection
