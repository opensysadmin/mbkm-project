<!doctype html>
<html lang="id">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{asset('css/app.css')}}" crossorigin="anonymous">

    <title>MBKM | {{ $title }}</title>
  </head>
  <body>
    <div class="container">
        <div class="row mt-3">
            <div class="col-3">
                <h5>Logo Nanti di sini</h5>
            </div>
            <div class="col">
              <h1>Log Management System</h1>
            </div>
        </div>
        <div class="row mt-5">
          <div class="col-3">
            <nav class="nav flex-column">
              <a class="nav-link active" aria-current="page" href="#">Perangkat</a>
              <a class="nav-link" href="#">Profil</a>
              <a class="nav-link" href="#">Kelola User</a>
              <a class="nav-link" href="#">Pengaturan</a>
            </nav>
          </div>

              @yield('container')
        </div>


    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

  </body>
</html>
