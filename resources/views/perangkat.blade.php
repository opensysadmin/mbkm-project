@extends('layouts.main')

@section('container')
  <div class="col container">
    <div class="row">
        <h1>Perangkat</h1>
      </div>
        @if (Auth::user()->role_id == 2)
        <div class="row mt-4">
            <div class="col">
                <a href="{{route('perangkat.tambah')}}" class="btn btn-success"><i class="bi bi-plus-square"></i> Tambah</a>
            </div>
        </div>
        @endif
      <div class="row mt-0">
        <table class="table table-striped table-hover table-sm" id="table-perangkat">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama Perangkat</th>
              <th scope="col">Tipe</th>
              <th scope="col">Brand</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($perangkat as $row)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$row->nama_perangkat}}</td>
                <td>{{$row->tipe->tipe}}</td>
                <td>{{$row->brand->brand}}</td>
                <td>
                    <a href="{{route('perangkat.detail',['id'=> $row->id])}}" class="btn btn-sm btn-primary" target="_blank"><i class="bi bi-eye"></i></a>
                    @if (Auth::user()->role_id == 2)
                    <a href="{{route('perangkat.edit',['id'=> $row->id])}}" class="btn btn-sm btn-warning"><i class="bi bi-pencil-square"></i></a>
                    <a class="btn btn-sm btn-danger" href="{{ route('perangkat.delete') }}"
                    onclick="event.preventDefault();
                                document.getElementById('hapus-form').submit();">
                    <i class="bi bi-trash-fill"></i>                        </a>
                        <form id="hapus-form" action="{{ route('perangkat.delete') }}" method="POST" class="d-none">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="id" value="{{$row->id}}">
                        </form>
                    @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
@endsection
@push('script')
<script>
    $(document).ready(function () {
    $('#table-perangkat').DataTable({
        dom: '<"toolbar">frtip',
        ordering: false,
        lengthChange: false,
        language: {
            "search": "Cari:",
            "emptyTable": "No data available in table",
            "infoEmpty":  "Menampilkan 0 dari 0 entri",
            "info":       "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "Selanjutnya",
                "previous":   "Sebelumnya"
            },
        }
    });
    $('div.toolbar').html('');

});
</script>
@endpush
