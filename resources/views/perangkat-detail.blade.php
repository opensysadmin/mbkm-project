@extends('layouts.main')

@section('container')
  <div class="col bg-light p-3 rounded">
      <div class="row">
        <div class="col-6">
          <h2>{{$perangkat->nama_perangkat}}</h2>
        </div>
        <div class="col-6 justify-content-end">
            @if(Auth::user()->role_id == 2)
            <p align="right"><a href="{{route('perangkat.edit',['id' => $perangkat->id])}}" class="btn btn-warning"><i class="bi bi-pencil-square"></i></a></p>
            @endif
        </div>
      </div>
      <div class="row mt-1">
        <div class="col col-xs-12 m-1 rounded bg-white">
          <div class="mb-0 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Nama Perangkat</label>
            <div class="col-sm-8">
              <label class="col-form-label">{{$perangkat->nama_perangkat}}</label>
            </div>
          </div>
          <div class="mb-0 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Serial Number</label>
            <div class="col-sm-8">
              <label class="col-form-label">{{$perangkat->serial_number}}</label>
            </div>
          </div>
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Brand</label>
            <div class="col-sm-8">
              <label class="col-form-label">{{$perangkat->brand->brand}}</label>
            </div>
          </div>
        </div>
        <div class="col col-xs-12 m-1 rounded bg-white">
          <div class="mb-0 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Lokasi</label>
            <div class="col-sm-8">
              <label class="col-form-label">{{$perangkat->lokasi->lokasi}}</label>
            </div>
          </div>
          <div class="mb-0 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Tipe</label></label>
            <div class="col-sm-8">
              <label class="col-form-label">{{$perangkat->tipe->tipe}}</label>
            </div>
          </div>
          <div class="mb-2 row">
            <label for="nama_perangkat" class="col-sm-4 col-form-label">Deskripsi</label>
            <div class="col-sm-8">
              <label class="col-form-label">{{$perangkat->deskripsi}}</label>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-2">
        <table class="table table-striped table-hover table-sm" id="log_table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Timestamp</th>
              <th scope="col">Facility</th>
              <th scope="col">Priority</th>
              <th scope="col">Tag</th>
              <th scope="col">Message</th>
            </tr>
          </thead>
          <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($syslog  as $row)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$row->ReceivedAt}}</td>
                <td>{{ $row->Facility }}</td>
                <td>{{ $row->Priority }}</td>
                <td>{{ $row->SysLogTag }}</td>
                <td>{{ $row->Message }}</td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>


  </div>
@endsection
@push('script')
<script>
    $(document).ready(function () {
    $('#log_table').DataTable({
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ordering: false,
        lengthChange: true,
        language: {
            "search": "Cari:",
            "emptyTable": "No data available in table",
            "infoEmpty":  "Menampilkan 0 dari 0 entri",
            "info":       "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "paginate": {
                "first":      "Awal",
                "last":       "Akhir",
                "next":       "Selanjutnya",
                "previous":   "Sebelumnya"
            },
        },
        pageLength: 30,
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        scrollY: "500px",

    });

});
</script>
@endpush
