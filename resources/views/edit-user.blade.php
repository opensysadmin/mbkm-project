@extends('layouts.main')

@section('container')
  <div class="col bg-light p-3 rounded">
    <form method="POST" action="{{route('user.update',['id' => $user->id])}}">
        @csrf
        @method('PUT')
      <div class="row">
        <div class="col-sm-12">
          <h2>Edit User</h2>
        </div>
      </div>
      <div class="row mt-4">
        <div class="mb-3 row">
          <label for="email" class="col-sm-3 col-form-label">Email</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email" placeholder="email" value="{{$user->email}}" readonly>
          </div>
          @error('email')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
          <label for="name" class="col-sm-3 col-form-label">Nama</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="{{$user->name}}">
          </div>
          @error('name')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
            <label for="role" class="col-sm-3 col-form-label">Role</label>
            <div class="col-sm-9">
                <select class="form-select" id="role_id" name="role_id" aria-label="Default select example">
                    <option selected disabled>Role</option>
                    @foreach ($roles as $item)
                        <option value="{{$item->id}}" {{($item->id == $user->role_id) ? 'selected' : ''}}>{{$item->role}}</option>
                    @endforeach
                </select>
            </div>
            @error('role')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
          </div>
          <hr>
          <div class="row">
            <h6 class="text-danger">Isi untuk mereset password</h6>
          </div>
          <div class="mb-3 row">
            <label for="newPassword" class="col-sm-3 col-form-label">Password Baru</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="password" placeholder="Password Baru" name="password">
            </div>

          </div>
          <div class="mb-3 row">
            <label for="repeatPassword" class="col-sm-3 col-form-label">Ulangi Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="password_confirmation" placeholder="Ulangi Password Baru" name="password_confirmation">
            </div>
            @error('password')
                <div class="text-danger">
                  {{$message}}
                </div>
            @enderror
          </div>
        <div class="row mb-3">
          <div class="col-sm-3 align-self-end">
            <button class="btn btn-success" type="submit">Simpan</button>
          </div>
        </div>
      </div>
    </form>

  </div>
@endsection
