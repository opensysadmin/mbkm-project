@extends('layouts.main')

@section('container')
  <div class="col bg-light p-3 rounded">
    <form method="POST" action="{{route('perangkat.simpan')}}">
        @csrf
      <div class="row">
        <div class="col-sm-12">
          <h2>Tambah Perangkat</h2>
        </div>
      </div>
      <div class="row mt-4">
        <div class="mb-3 row">
          <label for="nama_perangkat" class="col-sm-3 col-form-label">Nama Perangkat</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="nama_perangkat" name="nama_perangkat" placeholder="Nama Perangkat">
          </div>
          @error('nama_perangkat')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
          <label for="serial_number" class="col-sm-3 col-form-label">Serial Number</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="serial_number" name="serial_number" placeholder="Serial Number">
          </div>
          @error('serial_number')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
            <label for="host" class="col-sm-3 col-form-label">Host</label>
            <div class="col-sm-9">
                <select class="form-select" id="host" name="host" aria-label="Default select example">
                    <option selected disabled>Host</option>
                    @foreach ($host as $item)
                        <option value="{{$item->FromHost}}">{{$item->FromHost}}</option>
                    @endforeach
                </select>
            </div>
            @error('host')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
          </div>
        <div class="mb-3 row">
          <label for="brand" class="col-sm-3 col-form-label">Brand</label>
          <div class="col-sm-9">
            <select class="form-select" id="brand" name="brand_id" aria-label="Default select example">
              <option selected disabled>Brand</option>
              @foreach ($brand as $item)
                    <option value="{{$item->id}}">{{$item->brand}}</option>
                @endforeach
            </select>
          </div>
          @error('brand_id')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
          <label for="lokasi" class="col-sm-3 col-form-label">Lokasi</label>
          <div class="col-sm-9">
            <select class="form-select" id="lokasi" name="lokasi_id" aria-label="Default select example">
              <option selected disabled>Lokasi</option>
              @foreach ($lokasi as $item)
                        <option value="{{$item->id}}">{{$item->lokasi}}</option>
                @endforeach
            </select>
          </div>
          @error('lokasi_id')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
          <label for="tipe" class="col-sm-3 col-form-label">Tipe</label>
          <div class="col-sm-9">
            <select class="form-select" id="tipe" name="tipe_id" aria-label="Default select example">
              <option selected disabled>Tipe</option>
              @foreach ($tipe as $item)
                        <option value="{{$item->id}}">{{$item->tipe}}</option>
                    @endforeach
            </select>
          </div>
          @error('tipe_id')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="mb-3 row">
          <label for="Deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
          <div class="col-sm-9">
            <textarea class="form-control" id="Deskripsi" name="deskripsi" rows="3"></textarea>
          </div>
          @error('deskripsi')
              <div class="text-danger">
                {{$message}}
              </div>
          @enderror
        </div>
        <div class="row mb-3">
          <div class="col-sm-3 align-self-end">
            <button class="btn btn-success" type="submit">Simpan</button>
          </div>
        </div>
      </div>
    </form>

  </div>
@endsection
