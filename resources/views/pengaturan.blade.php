@extends('layouts.main')

@section('container')
  <div class="col bg-light p-3 rounded">
    <div class="row">
      <div class="col-sm-6">
        <h2>Pengaturan</h2>
      </div>
    </div>


    <div class="row mt-4">

      <!--- BRAND --->
      <div class="col col-xs-12 rounded bg-white m-1 px-3 py-1">
        <form method="POST" action="{{route('pengaturan.simpan')}}">
            @csrf
            <input type="hidden" name="parameter" value="brand">
          <div class="row">
            <h5>Tambah Brand</h5>
          </div>
          <div class="row mb-1">
            <input type="text" class="form-control" name="brand" placeholder="Brand">
          </div>
          @error('brand')
              <div class="row mb-1 text-danger">
                    {{$message}}
              </div>
          @enderror
          <div class="row mb-1">
            <button tipe="submit" class="btn btn-success">Tambah</button>
          </div>
        </form>
        <div class="row">
          <table class="table table-striped table-hover table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Brand Perangkat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php
                  $i = 1;
              @endphp
              @forelse ($brand as $row)
              <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$row->brand}}</td>
                <td>
                    <form action="{{route('pengaturan.delete')}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="hidden" name="parameter" value="brand">
                    <input type="hidden" name="id_hapus" value="{{$row->id}}">
                    <button type="submit" class="btn btn-sm btn-danger">
                        <i class="bi bi-trash3-fill"></i>
                    </button>
                    </form>
                </td>
              </tr>
              @empty
              <tr>
                <td  colspan="3">Data kosong</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>

      <!--- Tipe --->
      <div class="col col-xs-12 rounded bg-white m-1 px-3 py-1">
        <form method="POST" action="{{route('pengaturan.simpan')}}">
            @csrf
            <input type="hidden" name="parameter" value="tipe">
          <div class="row">
            <h5>Tambah Tipe</h5>
          </div>
          <div class="row mb-1">
            <input type="text" class="form-control" name="tipe" placeholder="Tipe">
          </div>
          @error('tipe')
              <div class="row mb-1 text-danger">
                    {{$message}}
              </div>
          @enderror
          <div class="row mb-1">
            <button tipe="submit" class="btn btn-success">Tambah</button>
          </div>
        </form>
        <div class="row">
          <table class="table table-striped table-hover table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tipe Perangkat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @php
                  $i = 1;
              @endphp
              @forelse ($tipe as $row)
              <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$row->tipe}}</td>
                <td>
                    <form action="{{route('pengaturan.delete')}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="hidden" name="parameter" value="tipe">
                        <input type="hidden" name="id_hapus" value="{{$row->id}}">
                        <button type="submit" class="btn btn-sm btn-danger">
                            <i class="bi bi-trash3-fill"></i>
                        </button>
                    </form>
                </td>
              </tr>
              @empty
                  <tr>
                    <td colspan="3">Data kosong</td>
                  </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>

      <!--- Lokasi --->
      <div class="col col-xs-12 rounded bg-white m-1 px-3 py-1">
          <div class="row">
            <h5>Tambah Lokasi</h5>
          </div>
          <div class="row mb-1"><br>
            @error('lokasi')
            <div class="text-danger">
                  {{$message}}
            </div>
        @enderror
        @error('alamat')
            <div class="text-danger">
                  {{$message}}
            </div>
        @enderror
        </div>

          <div class="row mb-1">
            <button tipe="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#lokasiModal">Tambah</button>
          </div>
        <div class="row">
          <table class="table table-striped table-hover table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Lokasi</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @php
                $i = 1
            @endphp
              @forelse ($lokasi as $row)

              <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$row->lokasi}}</td>
                <td>
                    <form action="{{route('pengaturan.delete')}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="hidden" name="parameter" value="lokasi">
                        <input type="hidden" name="id_hapus" value="{{$row->id}}">
                        <button type="submit" class="btn btn-sm btn-danger">
                            <i class="bi bi-trash3-fill"></i>
                        </button>
                    </form>
                </td>
              </tr>
              @empty
              <tr>
                <td scope="row" colspan="3">Data Kosong</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>

    </div>

  </div>


  <div class="modal fade" id="lokasiModal" tabindex="-1" aria-labelledby="lokasiModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{route('pengaturan.simpan')}}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="lokasiModalLabel">Tambah Lokasi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="parameter" value="lokasi">
                <div class="mb-3">
                    <label for="lokasi" class="form-label">Lokasi</label>
                    <input type="text" name="lokasi" id="lokasi" class="form-control" placeholder="Nama Lokasi">
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat</label>
                    <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control" placeholder="Alamat Lokasi"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
