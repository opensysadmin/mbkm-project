<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PerangkatController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PengaturanController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

//Perangkat Controller
Route::get('/perangkat',[PerangkatController::class, 'index'])->name('perangkat');
Route::get('/perangkat/tambah',[PerangkatController::class, 'tambah'])->name('perangkat.tambah');
Route::get('/perangkat/{id}',[PerangkatController::class, 'detail'])->name('perangkat.detail');
Route::post('/perangkat',[PerangkatController::class, 'simpan'])->name('perangkat.simpan');
Route::get('/perangkat/{id}/edit',[PerangkatController::class, 'edit'])->name('perangkat.edit');
Route::put('/perangkat/{id}',[PerangkatController::class, 'update'])->name('perangkat.update');
Route::delete('/perangkat',[PerangkatController::class, 'delete'])->name('perangkat.delete');

//Profile
Route::get('/profile',[ProfileController::class, 'index'])->name('profile');
Route::put('/profile',[ProfileController::class, 'update'])->name('profile.update');

//User
Route::get('/user',[UserController::class, 'index'])->name('user');
Route::get('/user/tambah',[UserController::class, 'tambah'])->name('user.tambah');
Route::post('/user',[UserController::class, 'simpan'])->name('user.simpan');
Route::get('/user/{id}',[UserController::class, 'detail'])->name('user.detail');
Route::get('/user/{id}/edit',[UserController::class, 'edit'])->name('user.edit');
Route::put('/user/{id}',[UserController::class, 'update'])->name('user.update');

//Pengaturan
Route::get('/pengaturan',[PengaturanController::class, 'index'])->name('pengaturan');
Route::post('/pengaturan',[PengaturanController::class, 'simpan'])->name('pengaturan.simpan');
Route::put('/pengaturan',[PengaturanController::class, 'update'])->name('pengaturan.update');
Route::delete('/pengaturan',[PengaturanController::class, 'delete'])->name('pengaturan.delete');
