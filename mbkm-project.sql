-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Des 2022 pada 08.18
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mbkm-project`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `brands`
--

INSERT INTO `brands` (`id`, `brand`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Mikrotik', 2, '2022-12-25 22:22:27', '2022-12-25 22:22:27'),
(2, 'Alcatel', 2, '2022-12-25 22:22:47', '2022-12-25 22:22:47'),
(3, 'Cisco', 2, '2022-12-25 22:22:52', '2022-12-25 22:22:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasis`
--

CREATE TABLE `lokasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `lokasis`
--

INSERT INTO `lokasis` (`id`, `lokasi`, `alamat`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'IDC - SU257', 'IDC Duren Tiga\r\nJl. Duren Tiga Raya No.7, RT.9/RW.5, Duren Tiga, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12760', 2, '2022-12-25 22:08:36', '2022-12-25 22:08:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_01_151242_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2022_12_01_150139_create_brands_table', 1),
(7, '2022_12_01_150306_create_tipes_table', 1),
(8, '2022_12_01_150352_create_lokasis_table', 1),
(9, '2022_12_05_140559_create_perangkats_table', 2),
(10, '2022_12_06_140920_create_user_akses_perangkats_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perangkats`
--

CREATE TABLE `perangkats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_perangkat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `host` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `lokasi_id` bigint(20) UNSIGNED DEFAULT NULL,
  `add_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `perangkats`
--

INSERT INTO `perangkats` (`id`, `nama_perangkat`, `serial_number`, `host`, `deskripsi`, `tipe_id`, `brand_id`, `lokasi_id`, `add_by`, `created_at`, `updated_at`) VALUES
(1, 'Router MBKM', '15678909892874', '172.16.29.1', 'Router mbkm', 1, 1, 1, 1, '2022-12-26 17:16:54', '2022-12-26 17:16:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'user', '2022-12-23 01:06:47', '2022-12-23 01:06:47'),
(2, 'Admin', '2022-12-23 01:06:47', '2022-12-23 01:06:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `systemevents`
--

CREATE TABLE `systemevents` (
  `ID` int(10) UNSIGNED NOT NULL,
  `CustomerID` bigint(20) DEFAULT NULL,
  `ReceivedAt` datetime DEFAULT NULL,
  `DeviceReportedTime` datetime DEFAULT NULL,
  `Facility` smallint(6) DEFAULT NULL,
  `Priority` smallint(6) DEFAULT NULL,
  `FromHost` varchar(60) DEFAULT NULL,
  `Message` text DEFAULT NULL,
  `NTSeverity` int(11) DEFAULT NULL,
  `Importance` int(11) DEFAULT NULL,
  `EventSource` varchar(60) DEFAULT NULL,
  `EventUser` varchar(60) DEFAULT NULL,
  `EventCategory` int(11) DEFAULT NULL,
  `EventID` int(11) DEFAULT NULL,
  `EventBinaryData` text DEFAULT NULL,
  `MaxAvailable` int(11) DEFAULT NULL,
  `CurrUsage` int(11) DEFAULT NULL,
  `MinUsage` int(11) DEFAULT NULL,
  `MaxUsage` int(11) DEFAULT NULL,
  `InfoUnitID` int(11) DEFAULT NULL,
  `SysLogTag` varchar(60) DEFAULT NULL,
  `EventLogType` varchar(60) DEFAULT NULL,
  `GenericFileName` varchar(60) DEFAULT NULL,
  `SystemID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `systemevents`
--

INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(1, NULL, '2022-11-09 08:17:38', '2022-11-09 08:17:38', 1, 5, '172.16.29.1', ' user admin logged in from 182.2.166.75 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(2, NULL, '2022-11-09 08:17:40', '2022-11-09 08:17:40', 1, 5, '172.16.29.1', ' user admin logged in from 182.2.166.75 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(3, NULL, '2022-11-09 08:18:05', '2022-11-09 08:18:05', 1, 5, '172.16.29.1', ' Test remote syslog', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'script,info', NULL, NULL, NULL),
(4, NULL, '2022-11-09 08:46:06', '2022-11-09 08:46:06', 1, 5, '172.16.29.1', ' user admin logged out from 182.2.166.75 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(5, NULL, '2022-11-09 08:46:06', '2022-11-09 08:46:06', 1, 5, '172.16.29.1', ' user admin logged out from 182.2.166.75 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(6, NULL, '2022-11-09 08:46:14', '2022-11-09 08:46:14', 1, 5, '172.16.29.1', ' <l2tp-shofari>: terminating... - hungup', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(7, NULL, '2022-11-09 08:46:14', '2022-11-09 08:46:14', 1, 5, '172.16.29.1', ' shofari logged out, 2484 2545238 18088598 18064 22036 from 182.2.166.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(8, NULL, '2022-11-09 08:46:14', '2022-11-09 08:46:14', 1, 5, '172.16.29.1', ' <l2tp-shofari>: disconnected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(9, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(10, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(11, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[4500]<=>182.2.166.75[4500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(12, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' purging ISAKMP-SA 103.126.10.236[4500]<=>182.2.166.75[4500] spi=7aa86e226089afda:d9feeec0aa80d590.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(13, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-182.2.166.75[4500] spi:7aa86e226089afda:d9feeec0aa80d590 rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(14, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(15, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(16, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(17, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(18, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[4500]<=>182.2.166.75[4500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(19, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(20, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(21, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(22, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(23, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[4500]<=>182.2.166.75[4500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(24, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(25, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(26, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[4500]<=>182.2.166.75[4500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(27, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(28, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(29, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(30, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(31, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(32, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(33, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[4500]<=>182.2.166.75[4500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(34, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(35, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(36, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[4500]<=>182.2.166.75[4500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(37, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(38, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(39, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(40, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(41, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 182.2.166.75[4500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(42, NULL, '2022-11-09 08:46:48', '2022-11-09 08:46:48', 1, 5, '172.16.29.1', ' user admin logged in from 182.2.166.75 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(43, NULL, '2022-11-09 08:46:49', '2022-11-09 08:46:49', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-182.2.166.75[4500] spi:951c3a4668407502:acca472e0bea420b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(44, NULL, '2022-11-09 08:46:49', '2022-11-09 08:46:49', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-182.2.166.75[4500] spi:cf564d183933c0d5:ec5be31ebdfcf201', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(45, NULL, '2022-11-09 08:46:49', '2022-11-09 08:46:49', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-182.2.166.75[4500] spi:9b307acc4e0f3532:cc5b889b06479470', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(46, NULL, '2022-11-09 08:46:49', '2022-11-09 08:46:49', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-182.2.166.75[4500] spi:e81c07eccd3c35fc:a2005bb9d60cd858', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(47, NULL, '2022-11-09 08:46:49', '2022-11-09 08:46:49', 1, 5, '172.16.29.1', ' user admin logged in from 182.2.166.75 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(48, NULL, '2022-11-09 08:47:18', '2022-11-09 08:47:18', 1, 5, '172.16.29.1', ' user admin logged out from 182.2.166.75 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(49, NULL, '2022-11-09 08:47:18', '2022-11-09 08:47:18', 1, 5, '172.16.29.1', ' user admin logged out from 182.2.166.75 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(50, NULL, '2022-11-09 08:56:25', '2022-11-09 08:56:25', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(51, NULL, '2022-11-09 09:36:39', '2022-11-09 09:36:39', 1, 5, '172.16.29.1', ' user admin logged in from 66.96.225.127 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(52, NULL, '2022-11-09 09:36:40', '2022-11-09 09:36:40', 1, 5, '172.16.29.1', ' user admin logged in from 66.96.225.127 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(53, NULL, '2022-11-09 09:36:43', '2022-11-09 09:36:43', 10, 4, 'servermbkm', 'user denied: root (no-activity) from 66.96.225.127', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(54, NULL, '2022-11-09 09:51:32', '2022-11-09 09:51:32', 1, 5, '172.16.29.1', ' user admin logged out from 66.96.225.127 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(55, NULL, '2022-11-09 09:51:32', '2022-11-09 09:51:32', 1, 5, '172.16.29.1', ' user admin logged out from 66.96.225.127 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(56, NULL, '2022-11-09 10:30:29', '2022-11-09 10:30:29', 1, 5, '172.16.29.1', ' user admin logged in from 182.2.167.71 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(57, NULL, '2022-11-09 10:30:42', '2022-11-09 10:30:42', 1, 5, '172.16.29.1', ' user admin logged in from 182.2.167.71 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(58, NULL, '2022-11-09 10:31:20', '2022-11-09 10:31:20', 1, 5, '172.16.29.1', ' user admin logged out from 182.2.167.71 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(59, NULL, '2022-11-09 10:31:20', '2022-11-09 10:31:20', 1, 5, '172.16.29.1', ' user admin logged out from 182.2.167.71 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(60, NULL, '2022-11-09 10:39:50', '2022-11-09 10:39:50', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.76[15035]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(61, NULL, '2022-11-09 15:10:48', '2022-11-09 15:10:48', 1, 5, '172.16.29.1', ' ISAKMP-SA dying 103.126.10.236[4500]-182.2.166.75[4500] spi:9b307acc4e0f3532:cc5b889b06479470', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(62, NULL, '2022-11-09 15:10:48', '2022-11-09 15:10:48', 1, 5, '172.16.29.1', ' ISAKMP-SA dying 103.126.10.236[4500]-182.2.166.75[4500] spi:951c3a4668407502:acca472e0bea420b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(63, NULL, '2022-11-09 15:10:48', '2022-11-09 15:10:48', 1, 5, '172.16.29.1', ' ISAKMP-SA dying 103.126.10.236[4500]-182.2.166.75[4500] spi:cf564d183933c0d5:ec5be31ebdfcf201', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(64, NULL, '2022-11-09 15:10:48', '2022-11-09 15:10:48', 1, 5, '172.16.29.1', ' ISAKMP-SA dying 103.126.10.236[4500]-182.2.166.75[4500] spi:e81c07eccd3c35fc:a2005bb9d60cd858', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(65, NULL, '2022-11-09 16:46:48', '2022-11-09 16:46:48', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-182.2.166.75[4500] spi:951c3a4668407502:acca472e0bea420b rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(66, NULL, '2022-11-09 16:46:48', '2022-11-09 16:46:48', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-182.2.166.75[4500] spi:9b307acc4e0f3532:cc5b889b06479470 rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(67, NULL, '2022-11-09 16:46:48', '2022-11-09 16:46:48', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-182.2.166.75[4500] spi:cf564d183933c0d5:ec5be31ebdfcf201 rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(68, NULL, '2022-11-09 16:46:48', '2022-11-09 16:46:48', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-182.2.166.75[4500] spi:e81c07eccd3c35fc:a2005bb9d60cd858 rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(69, NULL, '2022-11-10 03:01:33', '2022-11-10 03:01:33', 2, 6, 'servermbkm', '2A9K1X5x024249: from=root, size=4260, class=0, nrcpts=1, msgid=<202211092001.2A9K1X5x024249@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24249]', NULL, NULL, NULL),
(70, NULL, '2022-11-10 03:01:33', '2022-11-10 03:01:33', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24249]', NULL, NULL, NULL),
(71, NULL, '2022-11-10 03:01:33', '2022-11-10 03:01:33', 2, 6, 'servermbkm', '2A9K1X5x024249: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2A9K1Xbc024274 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24249]', NULL, NULL, NULL),
(72, NULL, '2022-11-10 03:01:36', '2022-11-10 03:01:36', 2, 6, 'servermbkm', '2A9K1aHW024336: from=root, size=2783, class=0, nrcpts=1, msgid=<202211092001.2A9K1aHW024336@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24336]', NULL, NULL, NULL),
(73, NULL, '2022-11-10 03:01:36', '2022-11-10 03:01:36', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24336]', NULL, NULL, NULL),
(74, NULL, '2022-11-10 03:01:36', '2022-11-10 03:01:36', 2, 6, 'servermbkm', '2A9K1aHW024336: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2A9K1a8V024339 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24336]', NULL, NULL, NULL),
(75, NULL, '2022-11-10 08:19:50', '2022-11-10 08:19:50', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.99[16057]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(76, NULL, '2022-11-10 08:59:36', '2022-11-10 08:59:36', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(77, NULL, '2022-11-10 10:11:51', '2022-11-10 10:11:51', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.205.179[54858]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(78, NULL, '2022-11-10 19:40:36', '2022-11-10 19:40:36', 1, 5, '172.16.29.1', ' user jovial logged in from 103.139.10.145 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(79, NULL, '2022-11-10 19:50:46', '2022-11-10 19:50:46', 1, 5, '172.16.29.1', ' new script added by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(80, NULL, '2022-11-10 19:50:52', '2022-11-10 19:50:52', 1, 5, '172.16.29.1', ' created new share: pub', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'smb,info', NULL, NULL, NULL),
(81, NULL, '2022-11-10 19:51:52', '2022-11-10 19:51:52', 1, 5, '172.16.29.1', ' user jovial logged in from 103.139.10.145 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(82, NULL, '2022-11-10 19:54:10', '2022-11-10 19:54:10', 1, 5, '172.16.29.1', ' changed script settings by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(83, NULL, '2022-11-10 19:54:57', '2022-11-10 19:54:57', 1, 5, '172.16.29.1', ' changed script settings by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(84, NULL, '2022-11-10 19:58:31', '2022-11-10 19:58:31', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>103.139.10.145[4220]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(85, NULL, '2022-11-10 19:58:31', '2022-11-10 19:58:31', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-103.139.10.145[15189] spi:1a4e38fb1d55590a:a0b03ebb5332f03d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(86, NULL, '2022-11-10 19:58:31', '2022-11-10 19:58:31', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 103.139.10.145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(87, NULL, '2022-11-10 19:58:31', '2022-11-10 19:58:31', 1, 5, '172.16.29.1', ' jovial logged in, 172.16.28.10 from 103.139.10.145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(88, NULL, '2022-11-10 19:58:31', '2022-11-10 19:58:31', 1, 5, '172.16.29.1', ' <l2tp-jovial>: authenticated', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(89, NULL, '2022-11-10 19:58:31', '2022-11-10 19:58:31', 1, 5, '172.16.29.1', ' <l2tp-jovial>: connected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(90, NULL, '2022-11-10 19:58:33', '2022-11-10 19:58:33', 1, 5, '172.16.29.1', ' <l2tp-jovial>: terminating...', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(91, NULL, '2022-11-10 19:58:33', '2022-11-10 19:58:33', 1, 5, '172.16.29.1', ' jovial logged out, 2 12721 12557 70 45 from 103.139.10.145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(92, NULL, '2022-11-10 19:58:33', '2022-11-10 19:58:33', 1, 5, '172.16.29.1', ' <l2tp-jovial>: disconnected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(93, NULL, '2022-11-10 19:58:33', '2022-11-10 19:58:33', 1, 5, '172.16.29.1', ' purging ISAKMP-SA 103.126.10.236[4500]<=>103.139.10.145[15189] spi=1a4e38fb1d55590a:a0b03ebb5332f03d.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(94, NULL, '2022-11-10 19:58:33', '2022-11-10 19:58:33', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-103.139.10.145[15189] spi:1a4e38fb1d55590a:a0b03ebb5332f03d rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(95, NULL, '2022-11-10 19:59:02', '2022-11-10 19:59:02', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.237 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(96, NULL, '2022-11-10 19:59:03', '2022-11-10 19:59:03', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.237 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(97, NULL, '2022-11-10 19:59:07', '2022-11-10 19:59:07', 1, 5, '172.16.29.1', ' user jovial logged out from 103.139.10.145 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(98, NULL, '2022-11-10 19:59:07', '2022-11-10 19:59:07', 1, 5, '172.16.29.1', ' user jovial logged out from 103.139.10.145 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(99, NULL, '2022-11-10 20:08:31', '2022-11-10 20:08:31', 1, 5, '172.16.29.1', ' script removed by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(100, NULL, '2022-11-10 20:08:38', '2022-11-10 20:08:38', 1, 5, '172.16.29.1', ' new script added by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(101, NULL, '2022-11-10 20:18:08', '2022-11-10 20:18:08', 1, 5, '172.16.29.1', ' nat rule added by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(102, NULL, '2022-11-10 23:01:51', '2022-11-10 23:01:51', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.237 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(103, NULL, '2022-11-10 23:01:51', '2022-11-10 23:01:51', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.237 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(104, NULL, '2022-11-11 03:01:33', '2022-11-11 03:01:33', 2, 6, 'servermbkm', '2AAK1XNW026575: from=root, size=4495, class=0, nrcpts=1, msgid=<202211102001.2AAK1XNW026575@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[26575]', NULL, NULL, NULL),
(105, NULL, '2022-11-11 03:01:33', '2022-11-11 03:01:33', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[26575]', NULL, NULL, NULL),
(106, NULL, '2022-11-11 03:01:33', '2022-11-11 03:01:33', 2, 6, 'servermbkm', '2AAK1XNW026575: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34495, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AAK1X8s026600 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[26575]', NULL, NULL, NULL),
(107, NULL, '2022-11-11 03:01:36', '2022-11-11 03:01:36', 2, 6, 'servermbkm', '2AAK1aVN026662: from=root, size=3522, class=0, nrcpts=1, msgid=<202211102001.2AAK1aVN026662@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[26662]', NULL, NULL, NULL),
(108, NULL, '2022-11-11 03:01:36', '2022-11-11 03:01:36', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[26662]', NULL, NULL, NULL),
(109, NULL, '2022-11-11 03:01:36', '2022-11-11 03:01:36', 2, 6, 'servermbkm', '2AAK1aVN026662: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=33522, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AAK1asT026665 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[26662]', NULL, NULL, NULL),
(110, NULL, '2022-11-11 08:30:24', '2022-11-11 08:30:24', 1, 5, '172.16.29.1', ' sntp change time Nov/11/2022 01:31:11 => Nov/11/2022 01:31:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(111, NULL, '2022-11-11 09:38:05', '2022-11-11 09:38:05', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.151 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(112, NULL, '2022-11-11 09:38:23', '2022-11-11 09:38:23', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.151 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(113, NULL, '2022-11-11 10:33:23', '2022-11-11 10:33:23', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.151 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(114, NULL, '2022-11-11 10:33:23', '2022-11-11 10:33:23', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.151 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(115, NULL, '2022-11-11 15:36:43', '2022-11-11 15:36:43', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.65[63470]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(116, NULL, '2022-11-11 17:05:01', '2022-11-11 17:05:01', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>134.122.206.108[49975]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(117, NULL, '2022-11-11 17:18:54', '2022-11-11 17:18:54', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>134.122.206.108[56344]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(118, NULL, '2022-11-11 21:26:54', '2022-11-11 21:26:54', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>66.96.225.127[33166]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(119, NULL, '2022-11-11 21:26:54', '2022-11-11 21:26:54', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-66.96.225.127[37278] spi:7ac7ee5e02713bef:a397f50c31c649af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(120, NULL, '2022-11-11 21:26:54', '2022-11-11 21:26:54', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 66.96.225.127', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(121, NULL, '2022-11-11 21:26:54', '2022-11-11 21:26:54', 1, 5, '172.16.29.1', ' shofari logged in, 172.16.28.6 from 66.96.225.127', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(122, NULL, '2022-11-11 21:26:56', '2022-11-11 21:26:56', 1, 5, '172.16.29.1', ' <l2tp-shofari>: authenticated', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(123, NULL, '2022-11-11 21:26:56', '2022-11-11 21:26:56', 1, 5, '172.16.29.1', ' <l2tp-shofari>: connected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(124, NULL, '2022-11-11 21:39:25', '2022-11-11 21:39:25', 1, 5, '172.16.29.1', ' <l2tp-shofari>: terminating... - hungup', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(125, NULL, '2022-11-11 21:39:25', '2022-11-11 21:39:25', 1, 5, '172.16.29.1', ' shofari logged out, 751 2794242 48682853 27868 39152 from 66.96.225.127', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(126, NULL, '2022-11-11 21:39:25', '2022-11-11 21:39:25', 1, 5, '172.16.29.1', ' <l2tp-shofari>: disconnected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(127, NULL, '2022-11-12 03:23:11', '2022-11-12 03:23:11', 2, 6, 'servermbkm', '2ABKNBt2028862: from=root, size=4275, class=0, nrcpts=1, msgid=<202211112023.2ABKNBt2028862@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[28862]', NULL, NULL, NULL),
(128, NULL, '2022-11-12 03:23:11', '2022-11-12 03:23:11', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[28862]', NULL, NULL, NULL),
(129, NULL, '2022-11-12 03:23:11', '2022-11-12 03:23:11', 2, 6, 'servermbkm', '2ABKNBt2028862: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34275, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ABKNBtF028887 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[28862]', NULL, NULL, NULL),
(130, NULL, '2022-11-12 03:23:14', '2022-11-12 03:23:14', 2, 6, 'servermbkm', '2ABKNE3n028949: from=root, size=2783, class=0, nrcpts=1, msgid=<202211112023.2ABKNE3n028949@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[28949]', NULL, NULL, NULL),
(131, NULL, '2022-11-12 03:23:14', '2022-11-12 03:23:14', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[28949]', NULL, NULL, NULL),
(132, NULL, '2022-11-12 03:23:14', '2022-11-12 03:23:14', 2, 6, 'servermbkm', '2ABKNE3n028949: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ABKNEdi028952 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[28949]', NULL, NULL, NULL),
(133, NULL, '2022-11-12 03:50:54', '2022-11-12 03:50:54', 1, 5, '172.16.29.1', ' ISAKMP-SA dying 103.126.10.236[4500]-66.96.225.127[37278] spi:7ac7ee5e02713bef:a397f50c31c649af', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(134, NULL, '2022-11-12 04:15:22', '2022-11-12 04:15:22', 2, 6, 'servermbkm', '2ABLFMGm030101: from=root, size=182, class=0, nrcpts=1, msgid=<202211112115.2ABLFMGm030101@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30101]', NULL, NULL, NULL),
(135, NULL, '2022-11-12 04:15:22', '2022-11-12 04:15:22', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30101]', NULL, NULL, NULL),
(136, NULL, '2022-11-12 04:15:22', '2022-11-12 04:15:22', 2, 6, 'servermbkm', '2ABLFMGm030101: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=30182, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ABLFMY9030104 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30101]', NULL, NULL, NULL),
(137, NULL, '2022-11-12 05:26:54', '2022-11-12 05:26:54', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-66.96.225.127[37278] spi:7ac7ee5e02713bef:a397f50c31c649af rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(138, NULL, '2022-11-12 11:58:05', '2022-11-12 11:58:05', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.206.82[57087]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(139, NULL, '2022-11-12 16:35:02', '2022-11-12 16:35:02', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.134[37740]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(140, NULL, '2022-11-12 17:48:14', '2022-11-12 17:48:14', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>152.32.217.103[58940]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(141, NULL, '2022-11-12 18:05:23', '2022-11-12 18:05:23', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>152.32.217.103[59391]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(142, NULL, '2022-11-12 18:07:53', '2022-11-12 18:07:53', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>152.32.217.103[42759]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(143, NULL, '2022-11-12 22:14:54', '2022-11-12 22:14:54', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>152.32.148.84[52607]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(144, NULL, '2022-11-12 22:18:18', '2022-11-12 22:18:18', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>152.32.148.84[59554]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(145, NULL, '2022-11-12 22:19:32', '2022-11-12 22:19:32', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>152.32.148.84[58740]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(146, NULL, '2022-11-12 22:58:09', '2022-11-12 22:58:09', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 23.248.175.240', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(147, NULL, '2022-11-12 23:16:25', '2022-11-12 23:16:25', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 23.248.175.240', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(148, NULL, '2022-11-13 01:50:01', '2022-11-13 01:50:01', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.92', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(149, NULL, '2022-11-13 01:50:02', '2022-11-13 01:50:02', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.125', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(150, NULL, '2022-11-13 01:50:03', '2022-11-13 01:50:03', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(151, NULL, '2022-11-13 01:50:03', '2022-11-13 01:50:03', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(152, NULL, '2022-11-13 01:50:06', '2022-11-13 01:50:06', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 152.32.129.53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(153, NULL, '2022-11-13 01:50:09', '2022-11-13 01:50:09', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.211', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(154, NULL, '2022-11-13 01:50:18', '2022-11-13 01:50:18', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(155, NULL, '2022-11-13 03:01:34', '2022-11-13 03:01:34', 2, 6, 'servermbkm', '2ACK1YLZ037745: from=root, size=4260, class=0, nrcpts=1, msgid=<202211122001.2ACK1YLZ037745@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37745]', NULL, NULL, NULL),
(156, NULL, '2022-11-13 03:01:34', '2022-11-13 03:01:34', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37745]', NULL, NULL, NULL),
(157, NULL, '2022-11-13 03:01:34', '2022-11-13 03:01:34', 2, 6, 'servermbkm', '2ACK1YLZ037745: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ACK1Y9H037770 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37745]', NULL, NULL, NULL),
(158, NULL, '2022-11-13 03:01:37', '2022-11-13 03:01:37', 2, 6, 'servermbkm', '2ACK1bVi037832: from=root, size=2783, class=0, nrcpts=1, msgid=<202211122001.2ACK1bVi037832@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37832]', NULL, NULL, NULL),
(159, NULL, '2022-11-13 03:01:37', '2022-11-13 03:01:37', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37832]', NULL, NULL, NULL),
(160, NULL, '2022-11-13 03:01:37', '2022-11-13 03:01:37', 2, 6, 'servermbkm', '2ACK1bVi037832: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ACK1bn3037835 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37832]', NULL, NULL, NULL),
(161, NULL, '2022-11-13 12:49:36', '2022-11-13 12:49:36', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.5[13327]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(162, NULL, '2022-11-13 23:25:45', '2022-11-13 23:25:45', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>74.207.249.195[500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(163, NULL, '2022-11-14 02:44:22', '2022-11-14 02:44:22', 1, 5, '172.16.29.1', ' auth timeout', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ssh,info', NULL, NULL, NULL),
(164, NULL, '2022-11-14 03:01:35', '2022-11-14 03:01:35', 2, 6, 'servermbkm', '2ADK1Z1c039995: from=root, size=4260, class=0, nrcpts=1, msgid=<202211132001.2ADK1Z1c039995@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[39995]', NULL, NULL, NULL),
(165, NULL, '2022-11-14 03:01:35', '2022-11-14 03:01:35', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[39995]', NULL, NULL, NULL),
(166, NULL, '2022-11-14 03:01:35', '2022-11-14 03:01:35', 2, 6, 'servermbkm', '2ADK1Z1c039995: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ADK1ZL9040020 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[39995]', NULL, NULL, NULL),
(167, NULL, '2022-11-14 03:01:38', '2022-11-14 03:01:38', 2, 6, 'servermbkm', '2ADK1cQT040082: from=root, size=2783, class=0, nrcpts=1, msgid=<202211132001.2ADK1cQT040082@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40082]', NULL, NULL, NULL),
(168, NULL, '2022-11-14 03:01:38', '2022-11-14 03:01:38', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40082]', NULL, NULL, NULL),
(169, NULL, '2022-11-14 03:01:38', '2022-11-14 03:01:38', 2, 6, 'servermbkm', '2ADK1cQT040082: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ADK1clb040085 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40082]', NULL, NULL, NULL),
(170, NULL, '2022-11-14 03:44:08', '2022-11-14 03:44:08', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>202.196.65.124[55726]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(171, NULL, '2022-11-14 11:58:17', '2022-11-14 11:58:17', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>198.199.93.153[54666]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(172, NULL, '2022-11-14 13:00:35', '2022-11-14 13:00:35', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>65.49.20.96[46140]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(173, NULL, '2022-11-15 03:23:42', '2022-11-15 03:23:42', 2, 6, 'servermbkm', '2AEKNge9046597: from=root, size=4967, class=0, nrcpts=1, msgid=<202211142023.2AEKNge9046597@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[46597]', NULL, NULL, NULL),
(174, NULL, '2022-11-15 03:23:42', '2022-11-15 03:23:42', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[46597]', NULL, NULL, NULL),
(175, NULL, '2022-11-15 03:23:42', '2022-11-15 03:23:42', 2, 6, 'servermbkm', '2AEKNge9046597: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34967, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AEKNg6f046622 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[46597]', NULL, NULL, NULL),
(176, NULL, '2022-11-15 03:23:45', '2022-11-15 03:23:45', 2, 6, 'servermbkm', '2AEKNj3Y046684: from=root, size=2783, class=0, nrcpts=1, msgid=<202211142023.2AEKNj3Y046684@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[46684]', NULL, NULL, NULL),
(177, NULL, '2022-11-15 03:23:45', '2022-11-15 03:23:45', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[46684]', NULL, NULL, NULL),
(178, NULL, '2022-11-15 03:23:45', '2022-11-15 03:23:45', 2, 6, 'servermbkm', '2AEKNj3Y046684: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AEKNjS2046687 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[46684]', NULL, NULL, NULL),
(179, NULL, '2022-11-15 09:04:48', '2022-11-15 09:04:48', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(180, NULL, '2022-11-15 10:51:58', '2022-11-15 10:51:58', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.239[4135]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(181, NULL, '2022-11-16 03:01:33', '2022-11-16 03:01:33', 2, 6, 'servermbkm', '2AFK1XxJ048828: from=root, size=4260, class=0, nrcpts=1, msgid=<202211152001.2AFK1XxJ048828@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48828]', NULL, NULL, NULL),
(182, NULL, '2022-11-16 03:01:33', '2022-11-16 03:01:33', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48828]', NULL, NULL, NULL),
(183, NULL, '2022-11-16 03:01:33', '2022-11-16 03:01:33', 2, 6, 'servermbkm', '2AFK1XxJ048828: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AFK1XCK048853 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48828]', NULL, NULL, NULL),
(184, NULL, '2022-11-16 03:01:36', '2022-11-16 03:01:36', 2, 6, 'servermbkm', '2AFK1aFp048915: from=root, size=2783, class=0, nrcpts=1, msgid=<202211152001.2AFK1aFp048915@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48915]', NULL, NULL, NULL),
(185, NULL, '2022-11-16 03:01:36', '2022-11-16 03:01:36', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48915]', NULL, NULL, NULL);
INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(186, NULL, '2022-11-16 03:01:37', '2022-11-16 03:01:37', 2, 6, 'servermbkm', '2AFK1aFp048915: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AFK1aDG048918 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48915]', NULL, NULL, NULL),
(187, NULL, '2022-11-16 06:16:58', '2022-11-16 06:16:58', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>43.129.39.176[500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(188, NULL, '2022-11-16 06:16:58', '2022-11-16 06:16:58', 1, 5, '172.16.29.1', ' the packet is retransmitted by 43.129.39.176[500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(189, NULL, '2022-11-16 07:31:19', '2022-11-16 07:31:19', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>216.218.206.86[31767]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(190, NULL, '2022-11-16 09:28:18', '2022-11-16 09:28:18', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(191, NULL, '2022-11-16 13:21:01', '2022-11-16 13:21:01', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.203.138[58382]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(192, NULL, '2022-11-16 20:54:27', '2022-11-16 20:54:27', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.237 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(193, NULL, '2022-11-16 20:54:27', '2022-11-16 20:54:27', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.237 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(194, NULL, '2022-11-16 20:54:33', '2022-11-16 20:54:33', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.237 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(195, NULL, '2022-11-16 21:00:16', '2022-11-16 21:00:16', 1, 5, '172.16.29.1', ' changed script settings by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(196, NULL, '2022-11-16 21:02:44', '2022-11-16 21:02:44', 1, 5, 'info', ' file \"disk/RouterOS-2022nov16.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(197, NULL, '2022-11-16 21:17:36', '2022-11-16 21:17:36', 1, 5, '172.16.29.1', ' new script scheduled by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(198, NULL, '2022-11-16 21:31:29', '2022-11-16 21:31:29', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.237 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(199, NULL, '2022-11-17 03:01:33', '2022-11-17 03:01:33', 2, 6, 'servermbkm', '2AGK1XqX051095: from=root, size=4260, class=0, nrcpts=1, msgid=<202211162001.2AGK1XqX051095@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[51095]', NULL, NULL, NULL),
(200, NULL, '2022-11-17 03:01:33', '2022-11-17 03:01:33', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[51095]', NULL, NULL, NULL),
(201, NULL, '2022-11-17 03:01:33', '2022-11-17 03:01:33', 2, 6, 'servermbkm', '2AGK1XqX051095: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AGK1XjH051119 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[51095]', NULL, NULL, NULL),
(202, NULL, '2022-11-17 03:01:36', '2022-11-17 03:01:36', 2, 6, 'servermbkm', '2AGK1a4L051182: from=root, size=2783, class=0, nrcpts=1, msgid=<202211162001.2AGK1a4L051182@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[51182]', NULL, NULL, NULL),
(203, NULL, '2022-11-17 03:01:36', '2022-11-17 03:01:36', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[51182]', NULL, NULL, NULL),
(204, NULL, '2022-11-17 03:01:36', '2022-11-17 03:01:36', 2, 6, 'servermbkm', '2AGK1a4L051182: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AGK1aht051185 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[51182]', NULL, NULL, NULL),
(205, NULL, '2022-11-17 05:59:16', '2022-11-17 05:59:16', 1, 5, 'info', ' file \"disk/RouterOS-2022nov16.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(206, NULL, '2022-11-17 08:58:08', '2022-11-17 08:58:08', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(207, NULL, '2022-11-17 12:37:13', '2022-11-17 12:37:13', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.68[55079]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(208, NULL, '2022-11-17 22:08:54', '2022-11-17 22:08:54', 1, 5, '172.16.29.1', ' user jovial logged in from 103.139.10.56 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(209, NULL, '2022-11-17 22:09:58', '2022-11-17 22:09:58', 1, 5, '172.16.29.1', ' change time Nov/17/2022 15:10:54 => Nov/17/2022 15:10:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(210, NULL, '2022-11-17 22:09:58', '2022-11-17 22:09:58', 1, 5, '172.16.29.1', ' system time zone settings changed by jovial', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(211, NULL, '2022-11-17 22:11:20', '2022-11-17 22:11:20', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.237 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(212, NULL, '2022-11-17 22:11:42', '2022-11-17 22:11:42', 1, 5, '172.16.29.1', ' user jovial logged out from 103.139.10.56 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(213, NULL, '2022-11-17 22:15:49', '2022-11-17 22:15:49', 1, 5, '172.16.29.1', ' sntp change time Nov/17/2022 22:16:43 => Nov/17/2022 22:16:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(214, NULL, '2022-11-17 22:59:16', '2022-11-17 22:59:16', 1, 5, 'info', ' file \"disk/RouterOS-2022nov17.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(215, NULL, '2022-11-18 00:01:21', '2022-11-18 00:01:21', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.237 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(216, NULL, '2022-11-18 03:51:29', '2022-11-18 03:51:29', 2, 6, 'servermbkm', '2AHKpTbX053562: from=root, size=4275, class=0, nrcpts=1, msgid=<202211172051.2AHKpTbX053562@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53562]', NULL, NULL, NULL),
(217, NULL, '2022-11-18 03:51:29', '2022-11-18 03:51:29', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53562]', NULL, NULL, NULL),
(218, NULL, '2022-11-18 03:51:29', '2022-11-18 03:51:29', 2, 6, 'servermbkm', '2AHKpTbX053562: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34275, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AHKpT0j053586 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53562]', NULL, NULL, NULL),
(219, NULL, '2022-11-18 03:51:32', '2022-11-18 03:51:32', 2, 6, 'servermbkm', '2AHKpWfr053649: from=root, size=2783, class=0, nrcpts=1, msgid=<202211172051.2AHKpWfr053649@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53649]', NULL, NULL, NULL),
(220, NULL, '2022-11-18 03:51:32', '2022-11-18 03:51:32', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53649]', NULL, NULL, NULL),
(221, NULL, '2022-11-18 03:51:32', '2022-11-18 03:51:32', 2, 6, 'servermbkm', '2AHKpWfr053649: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AHKpW5u053652 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53649]', NULL, NULL, NULL),
(222, NULL, '2022-11-18 05:08:42', '2022-11-18 05:08:42', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 80.82.77.139', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(223, NULL, '2022-11-18 05:08:44', '2022-11-18 05:08:44', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 80.82.77.139', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(224, NULL, '2022-11-18 05:08:50', '2022-11-18 05:08:50', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 80.82.77.139', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(225, NULL, '2022-11-18 05:09:00', '2022-11-18 05:09:00', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 80.82.77.139', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(226, NULL, '2022-11-18 08:58:31', '2022-11-18 08:58:31', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(227, NULL, '2022-11-18 10:28:45', '2022-11-18 10:28:45', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>216.218.206.86[28932]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(228, NULL, '2022-11-18 13:18:47', '2022-11-18 13:18:47', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.212.118[33509]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(229, NULL, '2022-11-18 15:31:59', '2022-11-18 15:31:59', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>103.47.135.177[59054]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(230, NULL, '2022-11-18 15:31:59', '2022-11-18 15:31:59', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-103.47.135.177[1966] spi:65bc1786ffd48237:18ccb27f1fe2449f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(231, NULL, '2022-11-18 15:32:00', '2022-11-18 15:32:00', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 103.47.135.177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(232, NULL, '2022-11-18 15:32:00', '2022-11-18 15:32:00', 1, 5, '172.16.29.1', ' shofari logged in, 172.16.28.6 from 103.47.135.177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(233, NULL, '2022-11-18 15:32:00', '2022-11-18 15:32:00', 1, 5, '172.16.29.1', ' <l2tp-shofari>: authenticated', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(234, NULL, '2022-11-18 15:32:00', '2022-11-18 15:32:00', 1, 5, '172.16.29.1', ' <l2tp-shofari>: connected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(235, NULL, '2022-11-18 15:33:22', '2022-11-18 15:33:22', 1, 5, '172.16.29.1', ' <l2tp-shofari>: terminating...', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(236, NULL, '2022-11-18 15:33:22', '2022-11-18 15:33:22', 1, 5, '172.16.29.1', ' shofari logged out, 82 112387 226666 685 481 from 103.47.135.177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(237, NULL, '2022-11-18 15:33:22', '2022-11-18 15:33:22', 1, 5, '172.16.29.1', ' <l2tp-shofari>: disconnected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(238, NULL, '2022-11-18 15:33:22', '2022-11-18 15:33:22', 1, 5, '172.16.29.1', ' purging ISAKMP-SA 103.126.10.236[4500]<=>103.47.135.177[1966] spi=65bc1786ffd48237:18ccb27f1fe2449f.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(239, NULL, '2022-11-18 15:33:22', '2022-11-18 15:33:22', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-103.47.135.177[1966] spi:65bc1786ffd48237:18ccb27f1fe2449f rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(240, NULL, '2022-11-18 18:56:33', '2022-11-18 18:56:33', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>103.47.135.177[41842]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(241, NULL, '2022-11-18 18:56:33', '2022-11-18 18:56:33', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-103.47.135.177[8874] spi:db5d6affc32664fa:2c1748edd15b8137', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(242, NULL, '2022-11-18 18:56:34', '2022-11-18 18:56:34', 1, 5, '172.16.29.1', ' the packet is retransmitted by 103.47.135.177[8874].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(243, NULL, '2022-11-18 18:56:35', '2022-11-18 18:56:35', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 103.47.135.177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(244, NULL, '2022-11-18 18:56:35', '2022-11-18 18:56:35', 1, 5, '172.16.29.1', ' shofari logged in, 172.16.28.6 from 103.47.135.177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(245, NULL, '2022-11-18 18:56:35', '2022-11-18 18:56:35', 1, 5, '172.16.29.1', ' <l2tp-shofari>: authenticated', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(246, NULL, '2022-11-18 18:56:35', '2022-11-18 18:56:35', 1, 5, '172.16.29.1', ' <l2tp-shofari>: connected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(247, NULL, '2022-11-18 18:59:06', '2022-11-18 18:59:06', 4, 6, 'servermbkm', 'Received disconnect from 172.16.28.6 port 56738:11: disconnected by user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54774]', NULL, NULL, NULL),
(248, NULL, '2022-11-18 18:59:06', '2022-11-18 18:59:06', 4, 6, 'servermbkm', 'Disconnected from user shofari 172.16.28.6 port 56738', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54774]', NULL, NULL, NULL),
(249, NULL, '2022-11-18 19:00:22', '2022-11-18 19:00:22', 4, 6, 'servermbkm', 'Received disconnect from 172.16.28.6 port 55425:11: disconnected by user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54798]', NULL, NULL, NULL),
(250, NULL, '2022-11-18 19:00:22', '2022-11-18 19:00:22', 4, 6, 'servermbkm', 'Disconnected from user shofari 172.16.28.6 port 55425', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54798]', NULL, NULL, NULL),
(251, NULL, '2022-11-18 19:00:38', '2022-11-18 19:00:38', 4, 6, 'servermbkm', 'Received disconnect from 172.16.28.6 port 55438:11: disconnected by user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54810]', NULL, NULL, NULL),
(252, NULL, '2022-11-18 19:00:38', '2022-11-18 19:00:38', 4, 6, 'servermbkm', 'Disconnected from user shofari 172.16.28.6 port 55438', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54810]', NULL, NULL, NULL),
(253, NULL, '2022-11-18 19:00:58', '2022-11-18 19:00:58', 4, 6, 'servermbkm', 'Received disconnect from 172.16.28.6 port 56728:11: disconnected by user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54765]', NULL, NULL, NULL),
(254, NULL, '2022-11-18 19:00:58', '2022-11-18 19:00:58', 4, 6, 'servermbkm', 'Disconnected from user shofari 172.16.28.6 port 56728', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54765]', NULL, NULL, NULL),
(255, NULL, '2022-11-18 19:01:11', '2022-11-18 19:01:11', 4, 6, 'servermbkm', 'Received disconnect from 172.16.28.6 port 55460:11: disconnected by user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54822]', NULL, NULL, NULL),
(256, NULL, '2022-11-18 19:01:11', '2022-11-18 19:01:11', 4, 6, 'servermbkm', 'Disconnected from user shofari 172.16.28.6 port 55460', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54822]', NULL, NULL, NULL),
(257, NULL, '2022-11-18 19:05:57', '2022-11-18 19:05:57', 4, 6, 'servermbkm', 'Received disconnect from 172.16.28.6 port 55519:11: disconnected by user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54943]', NULL, NULL, NULL),
(258, NULL, '2022-11-18 19:05:57', '2022-11-18 19:05:57', 4, 6, 'servermbkm', 'Disconnected from user shofari 172.16.28.6 port 55519', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sshd[54943]', NULL, NULL, NULL),
(259, NULL, '2022-11-18 20:08:20', '2022-11-18 20:08:20', 1, 5, '172.16.29.1', ' <l2tp-shofari>: terminating...', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(260, NULL, '2022-11-18 20:08:20', '2022-11-18 20:08:20', 1, 5, '172.16.29.1', ' shofari logged out, 4305 19898024 437673977 175768 438720 from 103.47.135.177', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(261, NULL, '2022-11-18 20:08:20', '2022-11-18 20:08:20', 1, 5, '172.16.29.1', ' <l2tp-shofari>: disconnected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(262, NULL, '2022-11-18 20:08:20', '2022-11-18 20:08:20', 1, 5, '172.16.29.1', ' purging ISAKMP-SA 103.126.10.236[4500]<=>103.47.135.177[8874] spi=db5d6affc32664fa:2c1748edd15b8137.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(263, NULL, '2022-11-18 20:08:20', '2022-11-18 20:08:20', 1, 5, '172.16.29.1', ' ISAKMP-SA deleted 103.126.10.236[4500]-103.47.135.177[8874] spi:db5d6affc32664fa:2c1748edd15b8137 rekey:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(264, NULL, '2022-11-18 22:59:14', '2022-11-18 22:59:14', 1, 5, 'info', ' file \"disk/RouterOS-2022nov18.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(265, NULL, '2022-11-19 01:36:30', '2022-11-19 01:36:30', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>118.194.253.72[32875]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(266, NULL, '2022-11-19 01:39:51', '2022-11-19 01:39:51', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>118.194.253.72[54694]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(267, NULL, '2022-11-19 01:41:05', '2022-11-19 01:41:05', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>118.194.253.72[46169]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(268, NULL, '2022-11-19 03:01:58', '2022-11-19 03:01:58', 2, 6, 'servermbkm', '2AIK1w9C000903: from=root, size=5385, class=0, nrcpts=1, msgid=<202211182001.2AIK1w9C000903@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[903]', NULL, NULL, NULL),
(269, NULL, '2022-11-19 03:01:58', '2022-11-19 03:01:58', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[903]', NULL, NULL, NULL),
(270, NULL, '2022-11-19 03:01:58', '2022-11-19 03:01:58', 2, 6, 'servermbkm', '2AIK1w9C000903: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=35385, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AIK1wRG000928 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[903]', NULL, NULL, NULL),
(271, NULL, '2022-11-19 03:02:02', '2022-11-19 03:02:02', 2, 6, 'servermbkm', '2AIK22EJ000990: from=root, size=2783, class=0, nrcpts=1, msgid=<202211182002.2AIK22EJ000990@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[990]', NULL, NULL, NULL),
(272, NULL, '2022-11-19 03:02:02', '2022-11-19 03:02:02', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[990]', NULL, NULL, NULL),
(273, NULL, '2022-11-19 03:02:02', '2022-11-19 03:02:02', 2, 6, 'servermbkm', '2AIK22EJ000990: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AIK22Fn000993 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[990]', NULL, NULL, NULL),
(274, NULL, '2022-11-19 04:15:37', '2022-11-19 04:15:37', 2, 6, 'servermbkm', '2AILFbXs001235: from=root, size=182, class=0, nrcpts=1, msgid=<202211182115.2AILFbXs001235@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[1235]', NULL, NULL, NULL),
(275, NULL, '2022-11-19 04:15:37', '2022-11-19 04:15:37', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[1235]', NULL, NULL, NULL),
(276, NULL, '2022-11-19 04:15:37', '2022-11-19 04:15:37', 2, 6, 'servermbkm', '2AILFbXs001235: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=30182, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AILFbK0001238 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[1235]', NULL, NULL, NULL),
(277, NULL, '2022-11-19 04:26:59', '2022-11-19 04:26:59', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 107.150.117.169', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(278, NULL, '2022-11-19 04:45:19', '2022-11-19 04:45:19', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 107.150.117.169', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(279, NULL, '2022-11-19 09:28:21', '2022-11-19 09:28:21', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(280, NULL, '2022-11-19 18:08:49', '2022-11-19 18:08:49', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.21[15094]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(281, NULL, '2022-11-19 22:59:13', '2022-11-19 22:59:13', 1, 5, 'info', ' file \"disk/RouterOS-2022nov19.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(282, NULL, '2022-11-20 03:01:54', '2022-11-20 03:01:54', 2, 6, 'servermbkm', '2AJK1sFs003434: from=root, size=24577, class=0, nrcpts=1, msgid=<202211192001.2AJK1sFs003434@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[3434]', NULL, NULL, NULL),
(283, NULL, '2022-11-20 03:01:54', '2022-11-20 03:01:54', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[3434]', NULL, NULL, NULL),
(284, NULL, '2022-11-20 03:01:54', '2022-11-20 03:01:54', 2, 6, 'servermbkm', '2AJK1sFs003434: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=54577, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AJK1sTG003459 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[3434]', NULL, NULL, NULL),
(285, NULL, '2022-11-20 03:01:58', '2022-11-20 03:01:58', 2, 6, 'servermbkm', '2AJK1wmZ003521: from=root, size=2783, class=0, nrcpts=1, msgid=<202211192001.2AJK1wmZ003521@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[3521]', NULL, NULL, NULL),
(286, NULL, '2022-11-20 03:01:58', '2022-11-20 03:01:58', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[3521]', NULL, NULL, NULL),
(287, NULL, '2022-11-20 03:01:58', '2022-11-20 03:01:58', 2, 6, 'servermbkm', '2AJK1wmZ003521: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AJK1wTM003524 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[3521]', NULL, NULL, NULL),
(288, NULL, '2022-11-20 06:34:53', '2022-11-20 06:34:53', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.92', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(289, NULL, '2022-11-20 06:34:54', '2022-11-20 06:34:54', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(290, NULL, '2022-11-20 06:34:55', '2022-11-20 06:34:55', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.69', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(291, NULL, '2022-11-20 06:34:56', '2022-11-20 06:34:56', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 45.249.244.84', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(292, NULL, '2022-11-20 06:34:58', '2022-11-20 06:34:58', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(293, NULL, '2022-11-20 06:35:02', '2022-11-20 06:35:02', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.80', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(294, NULL, '2022-11-20 06:35:10', '2022-11-20 06:35:10', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.217', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(295, NULL, '2022-11-20 09:13:30', '2022-11-20 09:13:30', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(296, NULL, '2022-11-20 13:45:08', '2022-11-20 13:45:08', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.207.249[35922]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(297, NULL, '2022-11-20 16:34:06', '2022-11-20 16:34:06', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.72[20739]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(298, NULL, '2022-11-20 19:39:48', '2022-11-20 19:39:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>74.207.248.154[500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(299, NULL, '2022-11-20 22:59:12', '2022-11-20 22:59:12', 1, 5, 'info', ' file \"disk/RouterOS-2022nov20.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(300, NULL, '2022-11-21 03:06:33', '2022-11-21 03:06:33', 2, 6, 'servermbkm', '2AKK6XaV006625: from=root, size=4508, class=0, nrcpts=1, msgid=<202211202006.2AKK6XaV006625@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[6625]', NULL, NULL, NULL),
(301, NULL, '2022-11-21 03:06:33', '2022-11-21 03:06:33', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[6625]', NULL, NULL, NULL),
(302, NULL, '2022-11-21 03:06:33', '2022-11-21 03:06:33', 2, 6, 'servermbkm', '2AKK6XaV006625: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34508, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AKK6X3c006650 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[6625]', NULL, NULL, NULL),
(303, NULL, '2022-11-21 03:06:36', '2022-11-21 03:06:36', 2, 6, 'servermbkm', '2AKK6aqt006712: from=root, size=2783, class=0, nrcpts=1, msgid=<202211202006.2AKK6aqt006712@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[6712]', NULL, NULL, NULL),
(304, NULL, '2022-11-21 03:06:36', '2022-11-21 03:06:36', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[6712]', NULL, NULL, NULL),
(305, NULL, '2022-11-21 03:06:37', '2022-11-21 03:06:37', 2, 6, 'servermbkm', '2AKK6aqt006712: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AKK6aEO006715 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[6712]', NULL, NULL, NULL),
(306, NULL, '2022-11-21 03:46:39', '2022-11-21 03:46:39', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>202.196.65.124[45705]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(307, NULL, '2022-11-21 09:01:40', '2022-11-21 09:01:40', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(308, NULL, '2022-11-21 15:47:54', '2022-11-21 15:47:54', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>74.82.47.58[8205]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(309, NULL, '2022-11-21 22:59:11', '2022-11-21 22:59:11', 1, 5, 'info', ' file \"disk/RouterOS-2022nov21.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(310, NULL, '2022-11-22 03:01:42', '2022-11-22 03:01:42', 2, 6, 'servermbkm', '2ALK1gUV009805: from=root, size=4260, class=0, nrcpts=1, msgid=<202211212001.2ALK1gUV009805@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[9805]', NULL, NULL, NULL),
(311, NULL, '2022-11-22 03:01:42', '2022-11-22 03:01:42', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[9805]', NULL, NULL, NULL),
(312, NULL, '2022-11-22 03:01:42', '2022-11-22 03:01:42', 2, 6, 'servermbkm', '2ALK1gUV009805: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ALK1guw009830 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[9805]', NULL, NULL, NULL),
(313, NULL, '2022-11-22 03:01:46', '2022-11-22 03:01:46', 2, 6, 'servermbkm', '2ALK1k6l009892: from=root, size=2783, class=0, nrcpts=1, msgid=<202211212001.2ALK1k6l009892@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[9892]', NULL, NULL, NULL),
(314, NULL, '2022-11-22 03:01:46', '2022-11-22 03:01:46', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[9892]', NULL, NULL, NULL),
(315, NULL, '2022-11-22 03:01:46', '2022-11-22 03:01:46', 2, 6, 'servermbkm', '2ALK1k6l009892: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ALK1kem009895 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[9892]', NULL, NULL, NULL),
(316, NULL, '2022-11-22 09:01:36', '2022-11-22 09:01:36', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(317, NULL, '2022-11-22 16:12:34', '2022-11-22 16:12:34', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>65.49.20.90[26941]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(318, NULL, '2022-11-22 22:59:09', '2022-11-22 22:59:09', 1, 5, 'info', ' file \"disk/RouterOS-2022nov22.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(319, NULL, '2022-11-23 01:06:20', '2022-11-23 01:06:20', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.200.71[60753]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(320, NULL, '2022-11-23 03:39:38', '2022-11-23 03:39:38', 2, 6, 'servermbkm', '2AMKdcO9012106: from=root, size=4275, class=0, nrcpts=1, msgid=<202211222039.2AMKdcO9012106@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[12106]', NULL, NULL, NULL),
(321, NULL, '2022-11-23 03:39:38', '2022-11-23 03:39:38', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[12106]', NULL, NULL, NULL),
(322, NULL, '2022-11-23 03:39:38', '2022-11-23 03:39:38', 2, 6, 'servermbkm', '2AMKdcO9012106: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34275, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AMKdcHq012130 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[12106]', NULL, NULL, NULL),
(323, NULL, '2022-11-23 03:39:42', '2022-11-23 03:39:42', 2, 6, 'servermbkm', '2AMKdgqH012193: from=root, size=2783, class=0, nrcpts=1, msgid=<202211222039.2AMKdgqH012193@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[12193]', NULL, NULL, NULL),
(324, NULL, '2022-11-23 03:39:42', '2022-11-23 03:39:42', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[12193]', NULL, NULL, NULL),
(325, NULL, '2022-11-23 03:39:42', '2022-11-23 03:39:42', 2, 6, 'servermbkm', '2AMKdgqH012193: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AMKdgaV012196 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[12193]', NULL, NULL, NULL),
(326, NULL, '2022-11-23 09:03:39', '2022-11-23 09:03:39', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(327, NULL, '2022-11-23 14:06:17', '2022-11-23 14:06:17', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.166[4971]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(328, NULL, '2022-11-23 18:11:34', '2022-11-23 18:11:34', 1, 5, '172.16.29.1', ' sntp change time Nov/23/2022 18:12:36 => Nov/23/2022 18:12:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(329, NULL, '2022-11-23 21:41:59', '2022-11-23 21:41:59', 1, 5, '172.16.29.1', ' user admin logged in from 103.126.10.6 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(330, NULL, '2022-11-23 21:42:00', '2022-11-23 21:42:00', 1, 5, '172.16.29.1', ' user admin logged in from 103.126.10.6 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(331, NULL, '2022-11-23 21:42:18', '2022-11-23 21:42:18', 1, 5, '172.16.29.1', ' user admin logged out from 103.126.10.6 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(332, NULL, '2022-11-23 21:42:18', '2022-11-23 21:42:18', 1, 5, '172.16.29.1', ' user admin logged out from 103.126.10.6 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(333, NULL, '2022-11-23 21:42:22', '2022-11-23 21:42:22', 1, 5, '172.16.29.1', ' user admin logged in from 103.126.10.6 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(334, NULL, '2022-11-23 21:42:22', '2022-11-23 21:42:22', 1, 5, '172.16.29.1', ' user admin logged in from 103.126.10.6 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(335, NULL, '2022-11-23 21:42:23', '2022-11-23 21:42:23', 1, 5, '172.16.29.1', ' user admin logged out from 103.126.10.6 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(336, NULL, '2022-11-23 21:42:23', '2022-11-23 21:42:23', 1, 5, '172.16.29.1', ' user admin logged out from 103.126.10.6 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(337, NULL, '2022-11-23 22:59:09', '2022-11-23 22:59:09', 1, 5, 'info', ' file \"disk/RouterOS-2022nov23.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(338, NULL, '2022-11-24 03:01:43', '2022-11-24 03:01:43', 2, 6, 'servermbkm', '2ANK1hSL017117: from=root, size=146409, class=0, nrcpts=1, msgid=<202211232001.2ANK1hSL017117@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[17117]', NULL, NULL, NULL),
(339, NULL, '2022-11-24 03:01:43', '2022-11-24 03:01:43', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[17117]', NULL, NULL, NULL),
(340, NULL, '2022-11-24 03:01:43', '2022-11-24 03:01:43', 2, 6, 'servermbkm', '2ANK1hSL017117: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=176409, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ANK1hbe017142 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[17117]', NULL, NULL, NULL),
(341, NULL, '2022-11-24 03:01:47', '2022-11-24 03:01:47', 2, 6, 'servermbkm', '2ANK1lXt017204: from=root, size=2783, class=0, nrcpts=1, msgid=<202211232001.2ANK1lXt017204@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[17204]', NULL, NULL, NULL),
(342, NULL, '2022-11-24 03:01:47', '2022-11-24 03:01:47', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[17204]', NULL, NULL, NULL),
(343, NULL, '2022-11-24 03:01:47', '2022-11-24 03:01:47', 2, 6, 'servermbkm', '2ANK1lXt017204: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ANK1lfL017207 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[17204]', NULL, NULL, NULL),
(344, NULL, '2022-11-24 08:55:11', '2022-11-24 08:55:11', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(345, NULL, '2022-11-24 10:34:50', '2022-11-24 10:34:50', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.79[47617]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(346, NULL, '2022-11-24 22:59:08', '2022-11-24 22:59:08', 1, 5, 'info', ' file \"disk/RouterOS-2022nov24.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(347, NULL, '2022-11-25 03:01:42', '2022-11-25 03:01:42', 2, 6, 'servermbkm', '2AOK1g8G019388: from=root, size=4731, class=0, nrcpts=1, msgid=<202211242001.2AOK1g8G019388@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[19388]', NULL, NULL, NULL),
(348, NULL, '2022-11-25 03:01:42', '2022-11-25 03:01:42', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[19388]', NULL, NULL, NULL),
(349, NULL, '2022-11-25 03:01:42', '2022-11-25 03:01:42', 2, 6, 'servermbkm', '2AOK1g8G019388: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34731, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AOK1gUr019413 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[19388]', NULL, NULL, NULL),
(350, NULL, '2022-11-25 03:01:46', '2022-11-25 03:01:46', 2, 6, 'servermbkm', '2AOK1kK2019475: from=root, size=2783, class=0, nrcpts=1, msgid=<202211242001.2AOK1kK2019475@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[19475]', NULL, NULL, NULL),
(351, NULL, '2022-11-25 03:01:46', '2022-11-25 03:01:46', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[19475]', NULL, NULL, NULL),
(352, NULL, '2022-11-25 03:01:46', '2022-11-25 03:01:46', 2, 6, 'servermbkm', '2AOK1kK2019475: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AOK1kkX019478 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[19475]', NULL, NULL, NULL),
(353, NULL, '2022-11-25 03:02:09', '2022-11-25 03:02:09', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.92', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(354, NULL, '2022-11-25 03:02:56', '2022-11-25 03:02:56', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(355, NULL, '2022-11-25 03:02:56', '2022-11-25 03:02:56', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.74', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(356, NULL, '2022-11-25 03:02:56', '2022-11-25 03:02:56', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.83', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(357, NULL, '2022-11-25 03:02:56', '2022-11-25 03:02:56', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.83', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(358, NULL, '2022-11-25 03:03:02', '2022-11-25 03:03:02', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.216', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(359, NULL, '2022-11-25 03:03:03', '2022-11-25 03:03:03', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 154.89.5.215', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(360, NULL, '2022-11-25 04:11:36', '2022-11-25 04:11:36', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.212.89[55875]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(361, NULL, '2022-11-25 08:17:49', '2022-11-25 08:17:49', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.218[15849]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(362, NULL, '2022-11-25 08:55:40', '2022-11-25 08:55:40', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(363, NULL, '2022-11-25 22:59:06', '2022-11-25 22:59:06', 1, 5, 'info', ' file \"disk/RouterOS-2022nov25.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(364, NULL, '2022-11-26 00:02:05', '2022-11-26 00:02:05', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>128.1.48.107[52435]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(365, NULL, '2022-11-26 00:05:26', '2022-11-26 00:05:26', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>128.1.48.107[43563]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(366, NULL, '2022-11-26 00:06:40', '2022-11-26 00:06:40', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>128.1.48.107[55334]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(367, NULL, '2022-11-26 00:11:48', '2022-11-26 00:11:48', 1, 5, '172.16.29.1', ' sntp change time Nov/26/2022 00:12:53 => Nov/26/2022 00:12:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL);
INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(368, NULL, '2022-11-26 02:22:20', '2022-11-26 02:22:20', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 152.32.148.110', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(369, NULL, '2022-11-26 02:40:41', '2022-11-26 02:40:41', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 152.32.148.110', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(370, NULL, '2022-11-26 03:13:05', '2022-11-26 03:13:05', 2, 6, 'servermbkm', '2APKD5Yc021665: from=root, size=4275, class=0, nrcpts=1, msgid=<202211252013.2APKD5Yc021665@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21665]', NULL, NULL, NULL),
(371, NULL, '2022-11-26 03:13:05', '2022-11-26 03:13:05', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21665]', NULL, NULL, NULL),
(372, NULL, '2022-11-26 03:13:05', '2022-11-26 03:13:05', 2, 6, 'servermbkm', '2APKD5Yc021665: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34275, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2APKD5Zn021690 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21665]', NULL, NULL, NULL),
(373, NULL, '2022-11-26 03:13:09', '2022-11-26 03:13:09', 2, 6, 'servermbkm', '2APKD9UN021752: from=root, size=2783, class=0, nrcpts=1, msgid=<202211252013.2APKD9UN021752@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21752]', NULL, NULL, NULL),
(374, NULL, '2022-11-26 03:13:09', '2022-11-26 03:13:09', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21752]', NULL, NULL, NULL),
(375, NULL, '2022-11-26 03:13:09', '2022-11-26 03:13:09', 2, 6, 'servermbkm', '2APKD9UN021752: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2APKD9Q7021755 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21752]', NULL, NULL, NULL),
(376, NULL, '2022-11-26 04:15:30', '2022-11-26 04:15:30', 2, 6, 'servermbkm', '2APLFUBF021987: from=root, size=182, class=0, nrcpts=1, msgid=<202211252115.2APLFUBF021987@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21987]', NULL, NULL, NULL),
(377, NULL, '2022-11-26 04:15:30', '2022-11-26 04:15:30', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21987]', NULL, NULL, NULL),
(378, NULL, '2022-11-26 04:15:31', '2022-11-26 04:15:31', 2, 6, 'servermbkm', '2APLFUBF021987: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=30182, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2APLFUC8021990 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[21987]', NULL, NULL, NULL),
(379, NULL, '2022-11-26 09:00:26', '2022-11-26 09:00:26', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(380, NULL, '2022-11-26 15:10:25', '2022-11-26 15:10:25', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.220[12202]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(381, NULL, '2022-11-26 22:59:06', '2022-11-26 22:59:06', 1, 5, 'info', ' file \"disk/RouterOS-2022nov26.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(382, NULL, '2022-11-27 03:01:44', '2022-11-27 03:01:44', 2, 6, 'servermbkm', '2AQK1iuX024987: from=root, size=4493, class=0, nrcpts=1, msgid=<202211262001.2AQK1iuX024987@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24987]', NULL, NULL, NULL),
(383, NULL, '2022-11-27 03:01:44', '2022-11-27 03:01:44', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24987]', NULL, NULL, NULL),
(384, NULL, '2022-11-27 03:01:44', '2022-11-27 03:01:44', 2, 6, 'servermbkm', '2AQK1iuX024987: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34493, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AQK1i9v025011 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[24987]', NULL, NULL, NULL),
(385, NULL, '2022-11-27 03:01:48', '2022-11-27 03:01:48', 2, 6, 'servermbkm', '2AQK1mGF025074: from=root, size=2783, class=0, nrcpts=1, msgid=<202211262001.2AQK1mGF025074@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[25074]', NULL, NULL, NULL),
(386, NULL, '2022-11-27 03:01:48', '2022-11-27 03:01:48', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[25074]', NULL, NULL, NULL),
(387, NULL, '2022-11-27 03:01:48', '2022-11-27 03:01:48', 2, 6, 'servermbkm', '2AQK1mGF025074: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AQK1mZN025077 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[25074]', NULL, NULL, NULL),
(388, NULL, '2022-11-27 07:41:36', '2022-11-27 07:41:36', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>198.199.95.65[42797]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(389, NULL, '2022-11-27 09:26:54', '2022-11-27 09:26:54', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(390, NULL, '2022-11-27 14:31:21', '2022-11-27 14:31:21', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.12[55557]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(391, NULL, '2022-11-27 22:59:04', '2022-11-27 22:59:04', 1, 5, 'info', ' file \"disk/RouterOS-2022nov27.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(392, NULL, '2022-11-28 03:01:43', '2022-11-28 03:01:43', 2, 6, 'servermbkm', '2ARK1h9k027240: from=root, size=4730, class=0, nrcpts=1, msgid=<202211272001.2ARK1h9k027240@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[27240]', NULL, NULL, NULL),
(393, NULL, '2022-11-28 03:01:43', '2022-11-28 03:01:43', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[27240]', NULL, NULL, NULL),
(394, NULL, '2022-11-28 03:01:43', '2022-11-28 03:01:43', 2, 6, 'servermbkm', '2ARK1h9k027240: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34730, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ARK1h1A027265 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[27240]', NULL, NULL, NULL),
(395, NULL, '2022-11-28 03:01:47', '2022-11-28 03:01:47', 2, 6, 'servermbkm', '2ARK1l3t027327: from=root, size=2783, class=0, nrcpts=1, msgid=<202211272001.2ARK1l3t027327@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[27327]', NULL, NULL, NULL),
(396, NULL, '2022-11-28 03:01:47', '2022-11-28 03:01:47', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[27327]', NULL, NULL, NULL),
(397, NULL, '2022-11-28 03:01:47', '2022-11-28 03:01:47', 2, 6, 'servermbkm', '2ARK1l3t027327: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ARK1lV5027330 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[27327]', NULL, NULL, NULL),
(398, NULL, '2022-11-28 03:43:54', '2022-11-28 03:43:54', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>202.196.65.124[26698]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(399, NULL, '2022-11-28 08:54:33', '2022-11-28 08:54:33', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>45.56.87.134[500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(400, NULL, '2022-11-28 09:23:37', '2022-11-28 09:23:37', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(401, NULL, '2022-11-28 12:12:51', '2022-11-28 12:12:51', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.110[50387]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(402, NULL, '2022-11-28 22:59:02', '2022-11-28 22:59:02', 1, 5, 'info', ' file \"disk/RouterOS-2022nov28.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(403, NULL, '2022-11-29 03:36:37', '2022-11-29 03:36:37', 2, 6, 'servermbkm', '2ASKabDv030329: from=root, size=4746, class=0, nrcpts=1, msgid=<202211282036.2ASKabDv030329@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30329]', NULL, NULL, NULL),
(404, NULL, '2022-11-29 03:36:37', '2022-11-29 03:36:37', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30329]', NULL, NULL, NULL),
(405, NULL, '2022-11-29 03:36:37', '2022-11-29 03:36:37', 2, 6, 'servermbkm', '2ASKabDv030329: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34746, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ASKabN0030354 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30329]', NULL, NULL, NULL),
(406, NULL, '2022-11-29 03:36:41', '2022-11-29 03:36:41', 2, 6, 'servermbkm', '2ASKafZ1030438: from=root, size=2783, class=0, nrcpts=1, msgid=<202211282036.2ASKafZ1030438@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30438]', NULL, NULL, NULL),
(407, NULL, '2022-11-29 03:36:41', '2022-11-29 03:36:41', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30438]', NULL, NULL, NULL),
(408, NULL, '2022-11-29 03:36:41', '2022-11-29 03:36:41', 2, 6, 'servermbkm', '2ASKafZ1030438: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ASKaf7o030441 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[30438]', NULL, NULL, NULL),
(409, NULL, '2022-11-29 07:46:47', '2022-11-29 07:46:47', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.210.231[58374]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(410, NULL, '2022-11-29 08:57:38', '2022-11-29 08:57:38', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(411, NULL, '2022-11-29 14:22:18', '2022-11-29 14:22:18', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.22.179', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(412, NULL, '2022-11-29 14:22:57', '2022-11-29 14:22:57', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.230.132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(413, NULL, '2022-11-29 14:22:57', '2022-11-29 14:22:57', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.230.132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(414, NULL, '2022-11-29 14:22:57', '2022-11-29 14:22:57', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.230.132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(415, NULL, '2022-11-29 14:22:58', '2022-11-29 14:22:58', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.230.132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(416, NULL, '2022-11-29 14:22:58', '2022-11-29 14:22:58', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.230.132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(417, NULL, '2022-11-29 14:22:58', '2022-11-29 14:22:58', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 106.75.230.132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(418, NULL, '2022-11-29 17:40:25', '2022-11-29 17:40:25', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.134[65021]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(419, NULL, '2022-11-29 22:59:01', '2022-11-29 22:59:01', 1, 5, 'info', ' file \"disk/RouterOS-2022nov29.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(420, NULL, '2022-11-30 03:01:43', '2022-11-30 03:01:43', 2, 6, 'servermbkm', '2ATK1hC2032571: from=root, size=4260, class=0, nrcpts=1, msgid=<202211292001.2ATK1hC2032571@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[32571]', NULL, NULL, NULL),
(421, NULL, '2022-11-30 03:01:43', '2022-11-30 03:01:43', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[32571]', NULL, NULL, NULL),
(422, NULL, '2022-11-30 03:01:43', '2022-11-30 03:01:43', 2, 6, 'servermbkm', '2ATK1hC2032571: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ATK1hix032596 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[32571]', NULL, NULL, NULL),
(423, NULL, '2022-11-30 03:01:47', '2022-11-30 03:01:47', 2, 6, 'servermbkm', '2ATK1lj5032658: from=root, size=2783, class=0, nrcpts=1, msgid=<202211292001.2ATK1lj5032658@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[32658]', NULL, NULL, NULL),
(424, NULL, '2022-11-30 03:01:47', '2022-11-30 03:01:47', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[32658]', NULL, NULL, NULL),
(425, NULL, '2022-11-30 03:01:47', '2022-11-30 03:01:47', 2, 6, 'servermbkm', '2ATK1lj5032658: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2ATK1lLB032661 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[32658]', NULL, NULL, NULL),
(426, NULL, '2022-11-30 09:28:15', '2022-11-30 09:28:15', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(427, NULL, '2022-11-30 14:25:59', '2022-11-30 14:25:59', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.223[35754]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(428, NULL, '2022-11-30 22:59:00', '2022-11-30 22:59:00', 1, 5, 'info', ' file \"disk/RouterOS-2022nov30.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(429, NULL, '2022-12-01 03:01:42', '2022-12-01 03:01:42', 2, 6, 'servermbkm', '2AUK1ghB034827: from=root, size=4260, class=0, nrcpts=1, msgid=<202211302001.2AUK1ghB034827@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[34827]', NULL, NULL, NULL),
(430, NULL, '2022-12-01 03:01:42', '2022-12-01 03:01:42', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[34827]', NULL, NULL, NULL),
(431, NULL, '2022-12-01 03:01:43', '2022-12-01 03:01:43', 2, 6, 'servermbkm', '2AUK1ghB034827: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AUK1gL5034852 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[34827]', NULL, NULL, NULL),
(432, NULL, '2022-12-01 03:01:46', '2022-12-01 03:01:46', 2, 6, 'servermbkm', '2AUK1kdQ034914: from=root, size=2783, class=0, nrcpts=1, msgid=<202211302001.2AUK1kdQ034914@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[34914]', NULL, NULL, NULL),
(433, NULL, '2022-12-01 03:01:46', '2022-12-01 03:01:46', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[34914]', NULL, NULL, NULL),
(434, NULL, '2022-12-01 03:01:47', '2022-12-01 03:01:47', 2, 6, 'servermbkm', '2AUK1kdQ034914: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AUK1kUF034917 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[34914]', NULL, NULL, NULL),
(435, NULL, '2022-12-01 05:30:00', '2022-12-01 05:30:00', 2, 6, 'servermbkm', '2AUMU07l035205: from=root, size=366, class=0, nrcpts=1, msgid=<202211302230.2AUMU07l035205@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[35205]', NULL, NULL, NULL),
(436, NULL, '2022-12-01 05:30:00', '2022-12-01 05:30:00', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[35205]', NULL, NULL, NULL),
(437, NULL, '2022-12-01 05:30:00', '2022-12-01 05:30:00', 2, 6, 'servermbkm', '2AUMU07l035205: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=30366, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2AUMU055035208 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[35205]', NULL, NULL, NULL),
(438, NULL, '2022-12-01 07:31:35', '2022-12-01 07:31:35', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>184.105.247.247[26417]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(439, NULL, '2022-12-01 09:08:10', '2022-12-01 09:08:10', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(440, NULL, '2022-12-01 17:08:29', '2022-12-01 17:08:29', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 185.165.190.17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(441, NULL, '2022-12-01 17:08:31', '2022-12-01 17:08:31', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 185.165.190.17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(442, NULL, '2022-12-01 17:08:45', '2022-12-01 17:08:45', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 185.165.190.17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(443, NULL, '2022-12-01 22:58:59', '2022-12-01 22:58:59', 1, 5, 'info', ' file \"disk/RouterOS-2022dec01.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(444, NULL, '2022-12-02 03:17:32', '2022-12-02 03:17:32', 2, 6, 'servermbkm', '2B1KHWoN037219: from=root, size=4838, class=0, nrcpts=1, msgid=<202212012017.2B1KHWoN037219@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37219]', NULL, NULL, NULL),
(445, NULL, '2022-12-02 03:17:32', '2022-12-02 03:17:32', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37219]', NULL, NULL, NULL),
(446, NULL, '2022-12-02 03:17:33', '2022-12-02 03:17:33', 2, 6, 'servermbkm', '2B1KHWoN037219: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=34838, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B1KHW7f037244 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37219]', NULL, NULL, NULL),
(447, NULL, '2022-12-02 03:17:37', '2022-12-02 03:17:37', 2, 6, 'servermbkm', '2B1KHbwX037306: from=root, size=2783, class=0, nrcpts=1, msgid=<202212012017.2B1KHbwX037306@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37306]', NULL, NULL, NULL),
(448, NULL, '2022-12-02 03:17:37', '2022-12-02 03:17:37', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37306]', NULL, NULL, NULL),
(449, NULL, '2022-12-02 03:17:37', '2022-12-02 03:17:37', 2, 6, 'servermbkm', '2B1KHbwX037306: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B1KHbaA037309 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[37306]', NULL, NULL, NULL),
(450, NULL, '2022-12-02 09:11:44', '2022-12-02 09:11:44', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(451, NULL, '2022-12-02 14:06:37', '2022-12-02 14:06:37', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.44[59146]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(452, NULL, '2022-12-02 22:58:57', '2022-12-02 22:58:57', 1, 5, 'info', ' file \"disk/RouterOS-2022dec02.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(453, NULL, '2022-12-03 03:01:42', '2022-12-03 03:01:42', 2, 6, 'servermbkm', '2B2K1gM9040338: from=root, size=4260, class=0, nrcpts=1, msgid=<202212022001.2B2K1gM9040338@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40338]', NULL, NULL, NULL),
(454, NULL, '2022-12-03 03:01:42', '2022-12-03 03:01:42', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40338]', NULL, NULL, NULL),
(455, NULL, '2022-12-03 03:01:42', '2022-12-03 03:01:42', 2, 6, 'servermbkm', '2B2K1gM9040338: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34260, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B2K1gIC040362 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40338]', NULL, NULL, NULL),
(456, NULL, '2022-12-03 03:01:46', '2022-12-03 03:01:46', 2, 6, 'servermbkm', '2B2K1kO4040425: from=root, size=2783, class=0, nrcpts=1, msgid=<202212022001.2B2K1kO4040425@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40425]', NULL, NULL, NULL),
(457, NULL, '2022-12-03 03:01:46', '2022-12-03 03:01:46', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40425]', NULL, NULL, NULL),
(458, NULL, '2022-12-03 03:01:46', '2022-12-03 03:01:46', 2, 6, 'servermbkm', '2B2K1kO4040425: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B2K1k9M040428 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40425]', NULL, NULL, NULL),
(459, NULL, '2022-12-03 04:15:26', '2022-12-03 04:15:26', 2, 6, 'servermbkm', '2B2LFQxO040670: from=root, size=182, class=0, nrcpts=1, msgid=<202212022115.2B2LFQxO040670@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40670]', NULL, NULL, NULL),
(460, NULL, '2022-12-03 04:15:26', '2022-12-03 04:15:26', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40670]', NULL, NULL, NULL),
(461, NULL, '2022-12-03 04:15:26', '2022-12-03 04:15:26', 2, 6, 'servermbkm', '2B2LFQxO040670: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=30182, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B2LFQg1040673 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[40670]', NULL, NULL, NULL),
(462, NULL, '2022-12-03 08:55:01', '2022-12-03 08:55:01', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(463, NULL, '2022-12-03 10:14:42', '2022-12-03 10:14:42', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 152.32.201.129', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(464, NULL, '2022-12-03 10:33:01', '2022-12-03 10:33:01', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 152.32.201.129', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(465, NULL, '2022-12-03 12:29:49', '2022-12-03 12:29:49', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.204.44[60546]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(466, NULL, '2022-12-03 15:02:50', '2022-12-03 15:02:50', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>64.62.197.60[62650]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(467, NULL, '2022-12-03 22:58:56', '2022-12-03 22:58:56', 1, 5, 'info', ' file \"disk/RouterOS-2022dec03.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(468, NULL, '2022-12-04 03:01:44', '2022-12-04 03:01:44', 2, 6, 'servermbkm', '2B3K1ixM042750: from=root, size=4732, class=0, nrcpts=1, msgid=<202212032001.2B3K1ixM042750@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[42750]', NULL, NULL, NULL),
(469, NULL, '2022-12-04 03:01:44', '2022-12-04 03:01:44', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[42750]', NULL, NULL, NULL),
(470, NULL, '2022-12-04 03:01:44', '2022-12-04 03:01:44', 2, 6, 'servermbkm', '2B3K1ixM042750: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34732, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B3K1ilS042775 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[42750]', NULL, NULL, NULL),
(471, NULL, '2022-12-04 03:01:48', '2022-12-04 03:01:48', 2, 6, 'servermbkm', '2B3K1mnM042837: from=root, size=2783, class=0, nrcpts=1, msgid=<202212032001.2B3K1mnM042837@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[42837]', NULL, NULL, NULL),
(472, NULL, '2022-12-04 03:01:48', '2022-12-04 03:01:48', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[42837]', NULL, NULL, NULL),
(473, NULL, '2022-12-04 03:01:48', '2022-12-04 03:01:48', 2, 6, 'servermbkm', '2B3K1mnM042837: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B3K1mch042840 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[42837]', NULL, NULL, NULL),
(474, NULL, '2022-12-04 08:56:19', '2022-12-04 08:56:19', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(475, NULL, '2022-12-04 15:09:36', '2022-12-04 15:09:36', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>184.105.247.215[8828]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(476, NULL, '2022-12-04 21:36:45', '2022-12-04 21:36:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(477, NULL, '2022-12-04 21:36:46', '2022-12-04 21:36:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(478, NULL, '2022-12-04 21:36:46', '2022-12-04 21:36:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(479, NULL, '2022-12-04 21:36:46', '2022-12-04 21:36:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(480, NULL, '2022-12-04 21:36:46', '2022-12-04 21:36:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(481, NULL, '2022-12-04 21:36:47', '2022-12-04 21:36:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(482, NULL, '2022-12-04 21:36:47', '2022-12-04 21:36:47', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(483, NULL, '2022-12-04 21:36:47', '2022-12-04 21:36:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(484, NULL, '2022-12-04 21:36:47', '2022-12-04 21:36:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(485, NULL, '2022-12-04 21:36:48', '2022-12-04 21:36:48', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(486, NULL, '2022-12-04 21:36:48', '2022-12-04 21:36:48', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(487, NULL, '2022-12-04 21:36:48', '2022-12-04 21:36:48', 10, 4, 'servermbkm', 'user denied: ueer (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(488, NULL, '2022-12-04 21:36:48', '2022-12-04 21:36:48', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(489, NULL, '2022-12-04 21:36:49', '2022-12-04 21:36:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(490, NULL, '2022-12-04 21:36:49', '2022-12-04 21:36:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(491, NULL, '2022-12-04 21:36:49', '2022-12-04 21:36:49', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(492, NULL, '2022-12-04 21:36:49', '2022-12-04 21:36:49', 10, 4, 'servermbkm', 'user denied: nas (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(493, NULL, '2022-12-04 21:36:50', '2022-12-04 21:36:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(494, NULL, '2022-12-04 21:36:50', '2022-12-04 21:36:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(495, NULL, '2022-12-04 21:36:50', '2022-12-04 21:36:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(496, NULL, '2022-12-04 21:36:50', '2022-12-04 21:36:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(497, NULL, '2022-12-04 21:36:51', '2022-12-04 21:36:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(498, NULL, '2022-12-04 21:36:51', '2022-12-04 21:36:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(499, NULL, '2022-12-04 21:36:51', '2022-12-04 21:36:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(500, NULL, '2022-12-04 21:36:51', '2022-12-04 21:36:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(501, NULL, '2022-12-04 21:36:52', '2022-12-04 21:36:52', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(502, NULL, '2022-12-04 21:36:52', '2022-12-04 21:36:52', 10, 4, 'servermbkm', 'user denied: dbs (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(503, NULL, '2022-12-04 21:36:52', '2022-12-04 21:36:52', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(504, NULL, '2022-12-04 21:36:52', '2022-12-04 21:36:52', 10, 4, 'servermbkm', 'user denied: qnap (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(505, NULL, '2022-12-04 21:36:53', '2022-12-04 21:36:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(506, NULL, '2022-12-04 21:36:53', '2022-12-04 21:36:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(507, NULL, '2022-12-04 21:36:53', '2022-12-04 21:36:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(508, NULL, '2022-12-04 21:36:53', '2022-12-04 21:36:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(509, NULL, '2022-12-04 21:36:54', '2022-12-04 21:36:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(510, NULL, '2022-12-04 21:36:54', '2022-12-04 21:36:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(511, NULL, '2022-12-04 21:36:54', '2022-12-04 21:36:54', 10, 4, 'servermbkm', 'user denied: shop (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(512, NULL, '2022-12-04 21:36:54', '2022-12-04 21:36:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(513, NULL, '2022-12-04 21:36:55', '2022-12-04 21:36:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(514, NULL, '2022-12-04 21:36:55', '2022-12-04 21:36:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(515, NULL, '2022-12-04 21:36:55', '2022-12-04 21:36:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(516, NULL, '2022-12-04 21:36:55', '2022-12-04 21:36:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(517, NULL, '2022-12-04 21:36:56', '2022-12-04 21:36:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(518, NULL, '2022-12-04 21:36:56', '2022-12-04 21:36:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(519, NULL, '2022-12-04 21:36:56', '2022-12-04 21:36:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(520, NULL, '2022-12-04 21:36:56', '2022-12-04 21:36:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(521, NULL, '2022-12-04 21:36:57', '2022-12-04 21:36:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(522, NULL, '2022-12-04 21:36:57', '2022-12-04 21:36:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(523, NULL, '2022-12-04 21:36:57', '2022-12-04 21:36:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(524, NULL, '2022-12-04 21:36:57', '2022-12-04 21:36:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(525, NULL, '2022-12-04 21:36:57', '2022-12-04 21:36:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(526, NULL, '2022-12-04 21:36:58', '2022-12-04 21:36:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(527, NULL, '2022-12-04 21:36:58', '2022-12-04 21:36:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(528, NULL, '2022-12-04 21:36:58', '2022-12-04 21:36:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(529, NULL, '2022-12-04 21:36:58', '2022-12-04 21:36:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(530, NULL, '2022-12-04 21:36:59', '2022-12-04 21:36:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(531, NULL, '2022-12-04 21:36:59', '2022-12-04 21:36:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(532, NULL, '2022-12-04 21:36:59', '2022-12-04 21:36:59', 10, 4, 'servermbkm', 'user denied: pma (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(533, NULL, '2022-12-04 21:36:59', '2022-12-04 21:36:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(534, NULL, '2022-12-04 21:37:00', '2022-12-04 21:37:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(535, NULL, '2022-12-04 21:37:00', '2022-12-04 21:37:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(536, NULL, '2022-12-04 21:37:00', '2022-12-04 21:37:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(537, NULL, '2022-12-04 21:37:00', '2022-12-04 21:37:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(538, NULL, '2022-12-04 21:37:01', '2022-12-04 21:37:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(539, NULL, '2022-12-04 21:37:01', '2022-12-04 21:37:01', 10, 4, 'servermbkm', 'user denied: shopdb (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(540, NULL, '2022-12-04 21:37:01', '2022-12-04 21:37:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(541, NULL, '2022-12-04 21:37:01', '2022-12-04 21:37:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(542, NULL, '2022-12-04 21:37:02', '2022-12-04 21:37:02', 10, 4, 'servermbkm', 'user denied: wordspress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(543, NULL, '2022-12-04 21:37:02', '2022-12-04 21:37:02', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(544, NULL, '2022-12-04 21:37:02', '2022-12-04 21:37:02', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(545, NULL, '2022-12-04 21:37:02', '2022-12-04 21:37:02', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(546, NULL, '2022-12-04 21:37:03', '2022-12-04 21:37:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(547, NULL, '2022-12-04 21:37:03', '2022-12-04 21:37:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(548, NULL, '2022-12-04 21:37:03', '2022-12-04 21:37:03', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(549, NULL, '2022-12-04 21:37:03', '2022-12-04 21:37:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL);
INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(550, NULL, '2022-12-04 21:37:04', '2022-12-04 21:37:04', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(551, NULL, '2022-12-04 21:37:04', '2022-12-04 21:37:04', 10, 4, 'servermbkm', 'user denied: db (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(552, NULL, '2022-12-04 21:37:04', '2022-12-04 21:37:04', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(553, NULL, '2022-12-04 21:37:04', '2022-12-04 21:37:04', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(554, NULL, '2022-12-04 21:37:05', '2022-12-04 21:37:05', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(555, NULL, '2022-12-04 21:37:05', '2022-12-04 21:37:05', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(556, NULL, '2022-12-04 21:37:05', '2022-12-04 21:37:05', 10, 4, 'servermbkm', 'user denied: asustor (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(557, NULL, '2022-12-04 21:37:05', '2022-12-04 21:37:05', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(558, NULL, '2022-12-04 21:37:06', '2022-12-04 21:37:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(559, NULL, '2022-12-04 21:37:06', '2022-12-04 21:37:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(560, NULL, '2022-12-04 21:37:06', '2022-12-04 21:37:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(561, NULL, '2022-12-04 21:37:06', '2022-12-04 21:37:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(562, NULL, '2022-12-04 21:37:06', '2022-12-04 21:37:06', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(563, NULL, '2022-12-04 21:37:07', '2022-12-04 21:37:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(564, NULL, '2022-12-04 21:37:07', '2022-12-04 21:37:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(565, NULL, '2022-12-04 21:37:07', '2022-12-04 21:37:07', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(566, NULL, '2022-12-04 21:37:07', '2022-12-04 21:37:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(567, NULL, '2022-12-04 21:37:08', '2022-12-04 21:37:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(568, NULL, '2022-12-04 21:37:08', '2022-12-04 21:37:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(569, NULL, '2022-12-04 21:37:08', '2022-12-04 21:37:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(570, NULL, '2022-12-04 21:37:08', '2022-12-04 21:37:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(571, NULL, '2022-12-04 21:37:09', '2022-12-04 21:37:09', 10, 4, 'servermbkm', 'user denied: sql (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(572, NULL, '2022-12-04 21:37:09', '2022-12-04 21:37:09', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(573, NULL, '2022-12-04 21:37:09', '2022-12-04 21:37:09', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(574, NULL, '2022-12-04 21:37:09', '2022-12-04 21:37:09', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(575, NULL, '2022-12-04 21:37:10', '2022-12-04 21:37:10', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(576, NULL, '2022-12-04 21:37:10', '2022-12-04 21:37:10', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(577, NULL, '2022-12-04 21:37:10', '2022-12-04 21:37:10', 10, 4, 'servermbkm', 'user denied: database (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(578, NULL, '2022-12-04 21:37:10', '2022-12-04 21:37:10', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(579, NULL, '2022-12-04 21:37:11', '2022-12-04 21:37:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(580, NULL, '2022-12-04 21:37:11', '2022-12-04 21:37:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(581, NULL, '2022-12-04 21:37:11', '2022-12-04 21:37:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(582, NULL, '2022-12-04 21:37:11', '2022-12-04 21:37:11', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(583, NULL, '2022-12-04 21:37:12', '2022-12-04 21:37:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(584, NULL, '2022-12-04 21:37:12', '2022-12-04 21:37:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(585, NULL, '2022-12-04 21:37:12', '2022-12-04 21:37:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(586, NULL, '2022-12-04 21:37:12', '2022-12-04 21:37:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(587, NULL, '2022-12-04 21:37:13', '2022-12-04 21:37:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(588, NULL, '2022-12-04 21:37:13', '2022-12-04 21:37:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(589, NULL, '2022-12-04 21:37:13', '2022-12-04 21:37:13', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(590, NULL, '2022-12-04 21:37:13', '2022-12-04 21:37:13', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(591, NULL, '2022-12-04 21:37:14', '2022-12-04 21:37:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(592, NULL, '2022-12-04 21:37:14', '2022-12-04 21:37:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(593, NULL, '2022-12-04 21:37:14', '2022-12-04 21:37:14', 10, 4, 'servermbkm', 'user denied: root (empty-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(594, NULL, '2022-12-04 21:37:14', '2022-12-04 21:37:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(595, NULL, '2022-12-04 21:37:14', '2022-12-04 21:37:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(596, NULL, '2022-12-04 21:37:15', '2022-12-04 21:37:15', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(597, NULL, '2022-12-04 21:37:15', '2022-12-04 21:37:15', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(598, NULL, '2022-12-04 21:37:15', '2022-12-04 21:37:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(599, NULL, '2022-12-04 21:37:15', '2022-12-04 21:37:15', 10, 4, 'servermbkm', 'user denied: pma (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(600, NULL, '2022-12-04 21:37:16', '2022-12-04 21:37:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(601, NULL, '2022-12-04 21:37:16', '2022-12-04 21:37:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(602, NULL, '2022-12-04 21:37:16', '2022-12-04 21:37:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(603, NULL, '2022-12-04 21:37:16', '2022-12-04 21:37:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(604, NULL, '2022-12-04 21:37:17', '2022-12-04 21:37:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(605, NULL, '2022-12-04 21:37:17', '2022-12-04 21:37:17', 10, 4, 'servermbkm', 'user denied: dev (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(606, NULL, '2022-12-04 21:37:17', '2022-12-04 21:37:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(607, NULL, '2022-12-04 21:37:17', '2022-12-04 21:37:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(608, NULL, '2022-12-04 21:37:18', '2022-12-04 21:37:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(609, NULL, '2022-12-04 21:37:18', '2022-12-04 21:37:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(610, NULL, '2022-12-04 21:37:18', '2022-12-04 21:37:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(611, NULL, '2022-12-04 21:37:18', '2022-12-04 21:37:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(612, NULL, '2022-12-04 21:37:19', '2022-12-04 21:37:19', 10, 4, 'servermbkm', 'user denied: developer (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(613, NULL, '2022-12-04 21:37:19', '2022-12-04 21:37:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(614, NULL, '2022-12-04 21:37:19', '2022-12-04 21:37:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(615, NULL, '2022-12-04 21:37:19', '2022-12-04 21:37:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(616, NULL, '2022-12-04 21:37:20', '2022-12-04 21:37:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(617, NULL, '2022-12-04 21:37:20', '2022-12-04 21:37:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(618, NULL, '2022-12-04 21:37:20', '2022-12-04 21:37:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(619, NULL, '2022-12-04 21:37:20', '2022-12-04 21:37:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(620, NULL, '2022-12-04 21:37:21', '2022-12-04 21:37:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(621, NULL, '2022-12-04 21:37:21', '2022-12-04 21:37:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(622, NULL, '2022-12-04 21:37:21', '2022-12-04 21:37:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(623, NULL, '2022-12-04 21:37:21', '2022-12-04 21:37:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(624, NULL, '2022-12-04 21:37:22', '2022-12-04 21:37:22', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(625, NULL, '2022-12-04 21:37:22', '2022-12-04 21:37:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(626, NULL, '2022-12-04 21:37:22', '2022-12-04 21:37:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(627, NULL, '2022-12-04 21:37:22', '2022-12-04 21:37:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(628, NULL, '2022-12-04 21:37:22', '2022-12-04 21:37:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(629, NULL, '2022-12-04 21:37:23', '2022-12-04 21:37:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(630, NULL, '2022-12-04 21:37:23', '2022-12-04 21:37:23', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(631, NULL, '2022-12-04 21:37:23', '2022-12-04 21:37:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(632, NULL, '2022-12-04 21:37:23', '2022-12-04 21:37:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(633, NULL, '2022-12-04 21:37:24', '2022-12-04 21:37:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(634, NULL, '2022-12-04 21:37:24', '2022-12-04 21:37:24', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(635, NULL, '2022-12-04 21:37:24', '2022-12-04 21:37:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(636, NULL, '2022-12-04 21:37:24', '2022-12-04 21:37:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(637, NULL, '2022-12-04 21:37:25', '2022-12-04 21:37:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(638, NULL, '2022-12-04 21:37:25', '2022-12-04 21:37:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(639, NULL, '2022-12-04 21:37:25', '2022-12-04 21:37:25', 10, 4, 'servermbkm', 'user denied: web (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(640, NULL, '2022-12-04 21:37:25', '2022-12-04 21:37:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(641, NULL, '2022-12-04 21:37:26', '2022-12-04 21:37:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(642, NULL, '2022-12-04 21:37:26', '2022-12-04 21:37:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(643, NULL, '2022-12-04 21:37:26', '2022-12-04 21:37:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(644, NULL, '2022-12-04 21:37:26', '2022-12-04 21:37:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(645, NULL, '2022-12-04 21:37:27', '2022-12-04 21:37:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(646, NULL, '2022-12-04 21:37:27', '2022-12-04 21:37:27', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(647, NULL, '2022-12-04 21:37:27', '2022-12-04 21:37:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(648, NULL, '2022-12-04 21:37:27', '2022-12-04 21:37:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(649, NULL, '2022-12-04 21:37:28', '2022-12-04 21:37:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(650, NULL, '2022-12-04 21:37:28', '2022-12-04 21:37:28', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(651, NULL, '2022-12-04 21:37:28', '2022-12-04 21:37:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(652, NULL, '2022-12-04 21:37:28', '2022-12-04 21:37:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(653, NULL, '2022-12-04 21:37:29', '2022-12-04 21:37:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(654, NULL, '2022-12-04 21:37:29', '2022-12-04 21:37:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(655, NULL, '2022-12-04 21:37:29', '2022-12-04 21:37:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(656, NULL, '2022-12-04 21:37:29', '2022-12-04 21:37:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(657, NULL, '2022-12-04 21:37:30', '2022-12-04 21:37:30', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(658, NULL, '2022-12-04 21:37:30', '2022-12-04 21:37:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(659, NULL, '2022-12-04 21:37:30', '2022-12-04 21:37:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(660, NULL, '2022-12-04 21:37:30', '2022-12-04 21:37:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(661, NULL, '2022-12-04 21:37:31', '2022-12-04 21:37:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(662, NULL, '2022-12-04 21:37:31', '2022-12-04 21:37:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(663, NULL, '2022-12-04 21:37:31', '2022-12-04 21:37:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(664, NULL, '2022-12-04 21:37:31', '2022-12-04 21:37:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(665, NULL, '2022-12-04 21:37:32', '2022-12-04 21:37:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(666, NULL, '2022-12-04 21:37:32', '2022-12-04 21:37:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(667, NULL, '2022-12-04 21:37:32', '2022-12-04 21:37:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(668, NULL, '2022-12-04 21:37:32', '2022-12-04 21:37:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(669, NULL, '2022-12-04 21:37:33', '2022-12-04 21:37:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(670, NULL, '2022-12-04 21:37:33', '2022-12-04 21:37:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(671, NULL, '2022-12-04 21:37:33', '2022-12-04 21:37:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(672, NULL, '2022-12-04 21:37:33', '2022-12-04 21:37:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(673, NULL, '2022-12-04 21:37:34', '2022-12-04 21:37:34', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(674, NULL, '2022-12-04 21:37:34', '2022-12-04 21:37:34', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 103.105.197.2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(675, NULL, '2022-12-04 22:01:15', '2022-12-04 22:01:15', 1, 5, '172.16.29.1', ' sntp change time Dec/04/2022 22:02:30 => Dec/04/2022 22:02:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(676, NULL, '2022-12-04 22:31:15', '2022-12-04 22:31:15', 1, 5, '172.16.29.1', ' sntp change time Dec/04/2022 22:32:31 => Dec/04/2022 22:32:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(677, NULL, '2022-12-04 22:58:56', '2022-12-04 22:58:56', 1, 5, 'info', ' file \"disk/RouterOS-2022dec04.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(678, NULL, '2022-12-04 23:01:15', '2022-12-04 23:01:15', 1, 5, '172.16.29.1', ' sntp change time Dec/04/2022 23:02:30 => Dec/04/2022 23:02:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(679, NULL, '2022-12-05 03:16:16', '2022-12-05 03:16:16', 1, 5, '172.16.29.1', ' sntp change time Dec/05/2022 03:17:31 => Dec/05/2022 03:17:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(680, NULL, '2022-12-05 03:41:56', '2022-12-05 03:41:56', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>202.196.65.124[37182]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(681, NULL, '2022-12-05 03:47:31', '2022-12-05 03:47:31', 2, 6, 'servermbkm', '2B4KlVpe045290: from=root, size=4275, class=0, nrcpts=1, msgid=<202212042047.2B4KlVpe045290@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[45290]', NULL, NULL, NULL),
(682, NULL, '2022-12-05 03:47:31', '2022-12-05 03:47:31', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[45290]', NULL, NULL, NULL),
(683, NULL, '2022-12-05 03:47:31', '2022-12-05 03:47:31', 2, 6, 'servermbkm', '2B4KlVpe045290: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=34275, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B4KlVVV045314 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[45290]', NULL, NULL, NULL),
(684, NULL, '2022-12-05 03:47:35', '2022-12-05 03:47:35', 2, 6, 'servermbkm', '2B4KlZn5045377: from=root, size=2783, class=0, nrcpts=1, msgid=<202212042047.2B4KlZn5045377@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[45377]', NULL, NULL, NULL),
(685, NULL, '2022-12-05 03:47:35', '2022-12-05 03:47:35', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[45377]', NULL, NULL, NULL),
(686, NULL, '2022-12-05 03:47:35', '2022-12-05 03:47:35', 2, 6, 'servermbkm', '2B4KlZn5045377: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B4KlZlh045380 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[45377]', NULL, NULL, NULL),
(687, NULL, '2022-12-05 07:52:52', '2022-12-05 07:52:52', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>65.49.20.115[28051]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(688, NULL, '2022-12-05 09:15:45', '2022-12-05 09:15:45', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(689, NULL, '2022-12-05 12:29:46', '2022-12-05 12:29:46', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.204.111[34643]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(690, NULL, '2022-12-05 14:44:10', '2022-12-05 14:44:10', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(691, NULL, '2022-12-05 14:44:10', '2022-12-05 14:44:10', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(692, NULL, '2022-12-05 14:44:11', '2022-12-05 14:44:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(693, NULL, '2022-12-05 14:44:11', '2022-12-05 14:44:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(694, NULL, '2022-12-05 14:44:11', '2022-12-05 14:44:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(695, NULL, '2022-12-05 14:44:12', '2022-12-05 14:44:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(696, NULL, '2022-12-05 14:44:12', '2022-12-05 14:44:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(697, NULL, '2022-12-05 14:44:13', '2022-12-05 14:44:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(698, NULL, '2022-12-05 14:44:13', '2022-12-05 14:44:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(699, NULL, '2022-12-05 14:44:13', '2022-12-05 14:44:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(700, NULL, '2022-12-05 14:44:14', '2022-12-05 14:44:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(701, NULL, '2022-12-05 14:44:14', '2022-12-05 14:44:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(702, NULL, '2022-12-05 14:44:15', '2022-12-05 14:44:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(703, NULL, '2022-12-05 14:44:15', '2022-12-05 14:44:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(704, NULL, '2022-12-05 14:44:16', '2022-12-05 14:44:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(705, NULL, '2022-12-05 14:44:16', '2022-12-05 14:44:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(706, NULL, '2022-12-05 14:44:16', '2022-12-05 14:44:16', 10, 4, 'servermbkm', 'user denied: web (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(707, NULL, '2022-12-05 14:44:17', '2022-12-05 14:44:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(708, NULL, '2022-12-05 14:44:17', '2022-12-05 14:44:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(709, NULL, '2022-12-05 14:44:18', '2022-12-05 14:44:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(710, NULL, '2022-12-05 14:44:18', '2022-12-05 14:44:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(711, NULL, '2022-12-05 14:44:19', '2022-12-05 14:44:19', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(712, NULL, '2022-12-05 14:44:19', '2022-12-05 14:44:19', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(713, NULL, '2022-12-05 14:44:19', '2022-12-05 14:44:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(714, NULL, '2022-12-05 14:44:20', '2022-12-05 14:44:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(715, NULL, '2022-12-05 14:44:20', '2022-12-05 14:44:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(716, NULL, '2022-12-05 14:44:21', '2022-12-05 14:44:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(717, NULL, '2022-12-05 14:44:21', '2022-12-05 14:44:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(718, NULL, '2022-12-05 14:44:22', '2022-12-05 14:44:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(719, NULL, '2022-12-05 14:44:22', '2022-12-05 14:44:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(720, NULL, '2022-12-05 14:44:22', '2022-12-05 14:44:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(721, NULL, '2022-12-05 14:44:23', '2022-12-05 14:44:23', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(722, NULL, '2022-12-05 14:44:23', '2022-12-05 14:44:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(723, NULL, '2022-12-05 14:44:24', '2022-12-05 14:44:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(724, NULL, '2022-12-05 14:44:24', '2022-12-05 14:44:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(725, NULL, '2022-12-05 14:44:25', '2022-12-05 14:44:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(726, NULL, '2022-12-05 14:44:25', '2022-12-05 14:44:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(727, NULL, '2022-12-05 14:44:25', '2022-12-05 14:44:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(728, NULL, '2022-12-05 14:44:26', '2022-12-05 14:44:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(729, NULL, '2022-12-05 14:44:26', '2022-12-05 14:44:26', 10, 4, 'servermbkm', 'user denied: database (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(730, NULL, '2022-12-05 14:44:27', '2022-12-05 14:44:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(731, NULL, '2022-12-05 14:44:27', '2022-12-05 14:44:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(732, NULL, '2022-12-05 14:44:27', '2022-12-05 14:44:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(733, NULL, '2022-12-05 14:44:28', '2022-12-05 14:44:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(734, NULL, '2022-12-05 14:44:28', '2022-12-05 14:44:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(735, NULL, '2022-12-05 14:44:29', '2022-12-05 14:44:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(736, NULL, '2022-12-05 14:44:29', '2022-12-05 14:44:29', 10, 4, 'servermbkm', 'user denied: developer (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(737, NULL, '2022-12-05 14:44:30', '2022-12-05 14:44:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(738, NULL, '2022-12-05 14:44:30', '2022-12-05 14:44:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(739, NULL, '2022-12-05 14:44:30', '2022-12-05 14:44:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(740, NULL, '2022-12-05 14:44:31', '2022-12-05 14:44:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(741, NULL, '2022-12-05 14:44:31', '2022-12-05 14:44:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(742, NULL, '2022-12-05 14:44:32', '2022-12-05 14:44:32', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(743, NULL, '2022-12-05 14:44:32', '2022-12-05 14:44:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(744, NULL, '2022-12-05 14:44:33', '2022-12-05 14:44:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(745, NULL, '2022-12-05 14:44:33', '2022-12-05 14:44:33', 10, 4, 'servermbkm', 'user denied: shopdb (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(746, NULL, '2022-12-05 14:44:33', '2022-12-05 14:44:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(747, NULL, '2022-12-05 14:44:34', '2022-12-05 14:44:34', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(748, NULL, '2022-12-05 14:44:34', '2022-12-05 14:44:34', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(749, NULL, '2022-12-05 14:44:35', '2022-12-05 14:44:35', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(750, NULL, '2022-12-05 14:44:35', '2022-12-05 14:44:35', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(751, NULL, '2022-12-05 14:44:35', '2022-12-05 14:44:35', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(752, NULL, '2022-12-05 14:44:36', '2022-12-05 14:44:36', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL);
INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(753, NULL, '2022-12-05 14:44:36', '2022-12-05 14:44:36', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(754, NULL, '2022-12-05 14:44:37', '2022-12-05 14:44:37', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(755, NULL, '2022-12-05 14:44:37', '2022-12-05 14:44:37', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(756, NULL, '2022-12-05 14:44:38', '2022-12-05 14:44:38', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(757, NULL, '2022-12-05 14:44:38', '2022-12-05 14:44:38', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(758, NULL, '2022-12-05 14:44:38', '2022-12-05 14:44:38', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(759, NULL, '2022-12-05 14:44:39', '2022-12-05 14:44:39', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(760, NULL, '2022-12-05 14:44:39', '2022-12-05 14:44:39', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(761, NULL, '2022-12-05 14:44:40', '2022-12-05 14:44:40', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(762, NULL, '2022-12-05 14:44:40', '2022-12-05 14:44:40', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(763, NULL, '2022-12-05 14:44:41', '2022-12-05 14:44:41', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(764, NULL, '2022-12-05 14:44:41', '2022-12-05 14:44:41', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(765, NULL, '2022-12-05 14:44:41', '2022-12-05 14:44:41', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(766, NULL, '2022-12-05 14:44:42', '2022-12-05 14:44:42', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(767, NULL, '2022-12-05 14:44:42', '2022-12-05 14:44:42', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(768, NULL, '2022-12-05 14:44:43', '2022-12-05 14:44:43', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(769, NULL, '2022-12-05 14:44:43', '2022-12-05 14:44:43', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(770, NULL, '2022-12-05 14:44:44', '2022-12-05 14:44:44', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(771, NULL, '2022-12-05 14:44:44', '2022-12-05 14:44:44', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(772, NULL, '2022-12-05 14:44:44', '2022-12-05 14:44:44', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(773, NULL, '2022-12-05 14:44:45', '2022-12-05 14:44:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(774, NULL, '2022-12-05 14:44:45', '2022-12-05 14:44:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(775, NULL, '2022-12-05 14:44:46', '2022-12-05 14:44:46', 10, 4, 'servermbkm', 'user denied: root (empty-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(776, NULL, '2022-12-05 14:44:46', '2022-12-05 14:44:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(777, NULL, '2022-12-05 14:44:47', '2022-12-05 14:44:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(778, NULL, '2022-12-05 14:44:47', '2022-12-05 14:44:47', 10, 4, 'servermbkm', 'user denied: nas (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(779, NULL, '2022-12-05 14:44:47', '2022-12-05 14:44:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(780, NULL, '2022-12-05 14:44:48', '2022-12-05 14:44:48', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(781, NULL, '2022-12-05 14:44:48', '2022-12-05 14:44:48', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(782, NULL, '2022-12-05 14:44:49', '2022-12-05 14:44:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(783, NULL, '2022-12-05 14:44:49', '2022-12-05 14:44:49', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(784, NULL, '2022-12-05 14:44:49', '2022-12-05 14:44:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(785, NULL, '2022-12-05 14:44:50', '2022-12-05 14:44:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(786, NULL, '2022-12-05 14:44:50', '2022-12-05 14:44:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(787, NULL, '2022-12-05 14:44:51', '2022-12-05 14:44:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(788, NULL, '2022-12-05 14:44:51', '2022-12-05 14:44:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(789, NULL, '2022-12-05 14:44:52', '2022-12-05 14:44:52', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(790, NULL, '2022-12-05 14:44:52', '2022-12-05 14:44:52', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(791, NULL, '2022-12-05 14:44:52', '2022-12-05 14:44:52', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(792, NULL, '2022-12-05 14:44:53', '2022-12-05 14:44:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(793, NULL, '2022-12-05 14:44:53', '2022-12-05 14:44:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(794, NULL, '2022-12-05 14:44:54', '2022-12-05 14:44:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(795, NULL, '2022-12-05 14:44:54', '2022-12-05 14:44:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(796, NULL, '2022-12-05 14:44:55', '2022-12-05 14:44:55', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(797, NULL, '2022-12-05 14:44:55', '2022-12-05 14:44:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(798, NULL, '2022-12-05 14:44:55', '2022-12-05 14:44:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(799, NULL, '2022-12-05 14:44:56', '2022-12-05 14:44:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(800, NULL, '2022-12-05 14:44:56', '2022-12-05 14:44:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(801, NULL, '2022-12-05 14:44:57', '2022-12-05 14:44:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(802, NULL, '2022-12-05 14:44:57', '2022-12-05 14:44:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(803, NULL, '2022-12-05 14:44:57', '2022-12-05 14:44:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(804, NULL, '2022-12-05 14:44:58', '2022-12-05 14:44:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(805, NULL, '2022-12-05 14:44:58', '2022-12-05 14:44:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(806, NULL, '2022-12-05 14:44:59', '2022-12-05 14:44:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(807, NULL, '2022-12-05 14:44:59', '2022-12-05 14:44:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(808, NULL, '2022-12-05 14:45:00', '2022-12-05 14:45:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(809, NULL, '2022-12-05 14:45:00', '2022-12-05 14:45:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[18177]', NULL, NULL, NULL),
(810, NULL, '2022-12-05 14:45:01', '2022-12-05 14:45:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(811, NULL, '2022-12-05 14:45:01', '2022-12-05 14:45:01', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(812, NULL, '2022-12-05 14:45:01', '2022-12-05 14:45:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(813, NULL, '2022-12-05 14:45:02', '2022-12-05 14:45:02', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(814, NULL, '2022-12-05 14:45:02', '2022-12-05 14:45:02', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(815, NULL, '2022-12-05 14:45:03', '2022-12-05 14:45:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(816, NULL, '2022-12-05 14:45:03', '2022-12-05 14:45:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(817, NULL, '2022-12-05 14:45:03', '2022-12-05 14:45:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(818, NULL, '2022-12-05 14:45:04', '2022-12-05 14:45:04', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(819, NULL, '2022-12-05 14:45:04', '2022-12-05 14:45:04', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(820, NULL, '2022-12-05 14:45:05', '2022-12-05 14:45:05', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(821, NULL, '2022-12-05 14:45:05', '2022-12-05 14:45:05', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(822, NULL, '2022-12-05 14:45:06', '2022-12-05 14:45:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(823, NULL, '2022-12-05 14:45:06', '2022-12-05 14:45:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(824, NULL, '2022-12-05 14:45:06', '2022-12-05 14:45:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(825, NULL, '2022-12-05 14:45:07', '2022-12-05 14:45:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(826, NULL, '2022-12-05 14:45:07', '2022-12-05 14:45:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(827, NULL, '2022-12-05 14:45:08', '2022-12-05 14:45:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(828, NULL, '2022-12-05 14:45:08', '2022-12-05 14:45:08', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(829, NULL, '2022-12-05 14:45:08', '2022-12-05 14:45:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(830, NULL, '2022-12-05 14:45:09', '2022-12-05 14:45:09', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(831, NULL, '2022-12-05 14:45:09', '2022-12-05 14:45:09', 10, 4, 'servermbkm', 'user denied: pma (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(832, NULL, '2022-12-05 14:45:10', '2022-12-05 14:45:10', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(833, NULL, '2022-12-05 14:45:10', '2022-12-05 14:45:10', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(834, NULL, '2022-12-05 14:45:11', '2022-12-05 14:45:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(835, NULL, '2022-12-05 14:45:11', '2022-12-05 14:45:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(836, NULL, '2022-12-05 14:45:11', '2022-12-05 14:45:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(837, NULL, '2022-12-05 14:45:12', '2022-12-05 14:45:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(838, NULL, '2022-12-05 14:45:12', '2022-12-05 14:45:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(839, NULL, '2022-12-05 14:45:13', '2022-12-05 14:45:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(840, NULL, '2022-12-05 14:45:13', '2022-12-05 14:45:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(841, NULL, '2022-12-05 14:45:14', '2022-12-05 14:45:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(842, NULL, '2022-12-05 14:45:14', '2022-12-05 14:45:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(843, NULL, '2022-12-05 14:45:14', '2022-12-05 14:45:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(844, NULL, '2022-12-05 14:45:15', '2022-12-05 14:45:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(845, NULL, '2022-12-05 14:45:15', '2022-12-05 14:45:15', 10, 4, 'servermbkm', 'user denied: ueer (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(846, NULL, '2022-12-05 14:45:16', '2022-12-05 14:45:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(847, NULL, '2022-12-05 14:45:16', '2022-12-05 14:45:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(848, NULL, '2022-12-05 14:45:17', '2022-12-05 14:45:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(849, NULL, '2022-12-05 14:45:17', '2022-12-05 14:45:17', 10, 4, 'servermbkm', 'user denied: shop (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(850, NULL, '2022-12-05 14:45:17', '2022-12-05 14:45:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(851, NULL, '2022-12-05 14:45:18', '2022-12-05 14:45:18', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(852, NULL, '2022-12-05 14:45:18', '2022-12-05 14:45:18', 10, 4, 'servermbkm', 'user denied: sql (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(853, NULL, '2022-12-05 14:45:19', '2022-12-05 14:45:19', 10, 4, 'servermbkm', 'user denied: db (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(854, NULL, '2022-12-05 14:45:19', '2022-12-05 14:45:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(855, NULL, '2022-12-05 14:45:19', '2022-12-05 14:45:19', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(856, NULL, '2022-12-05 14:45:20', '2022-12-05 14:45:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(857, NULL, '2022-12-05 14:45:20', '2022-12-05 14:45:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(858, NULL, '2022-12-05 14:45:21', '2022-12-05 14:45:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(859, NULL, '2022-12-05 14:45:21', '2022-12-05 14:45:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(860, NULL, '2022-12-05 14:45:22', '2022-12-05 14:45:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(861, NULL, '2022-12-05 14:45:22', '2022-12-05 14:45:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(862, NULL, '2022-12-05 14:45:23', '2022-12-05 14:45:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(863, NULL, '2022-12-05 14:45:23', '2022-12-05 14:45:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(864, NULL, '2022-12-05 14:45:23', '2022-12-05 14:45:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(865, NULL, '2022-12-05 14:45:24', '2022-12-05 14:45:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(866, NULL, '2022-12-05 14:45:24', '2022-12-05 14:45:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(867, NULL, '2022-12-05 14:45:25', '2022-12-05 14:45:25', 10, 4, 'servermbkm', 'user denied: wordspress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(868, NULL, '2022-12-05 14:45:25', '2022-12-05 14:45:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(869, NULL, '2022-12-05 14:45:25', '2022-12-05 14:45:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(870, NULL, '2022-12-05 14:45:26', '2022-12-05 14:45:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(871, NULL, '2022-12-05 14:45:26', '2022-12-05 14:45:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(872, NULL, '2022-12-05 14:45:27', '2022-12-05 14:45:27', 10, 4, 'servermbkm', 'user denied: dbs (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(873, NULL, '2022-12-05 14:45:27', '2022-12-05 14:45:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(874, NULL, '2022-12-05 14:45:28', '2022-12-05 14:45:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(875, NULL, '2022-12-05 14:45:28', '2022-12-05 14:45:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(876, NULL, '2022-12-05 14:45:28', '2022-12-05 14:45:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(877, NULL, '2022-12-05 14:45:29', '2022-12-05 14:45:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(878, NULL, '2022-12-05 14:45:29', '2022-12-05 14:45:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(879, NULL, '2022-12-05 14:45:30', '2022-12-05 14:45:30', 10, 4, 'servermbkm', 'user denied: qnap (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(880, NULL, '2022-12-05 14:45:30', '2022-12-05 14:45:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(881, NULL, '2022-12-05 14:45:31', '2022-12-05 14:45:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(882, NULL, '2022-12-05 14:45:31', '2022-12-05 14:45:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(883, NULL, '2022-12-05 14:45:31', '2022-12-05 14:45:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(884, NULL, '2022-12-05 14:45:32', '2022-12-05 14:45:32', 10, 4, 'servermbkm', 'user denied: pma (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(885, NULL, '2022-12-05 14:45:32', '2022-12-05 14:45:32', 10, 4, 'servermbkm', 'user denied: asustor (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(886, NULL, '2022-12-05 14:45:33', '2022-12-05 14:45:33', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(887, NULL, '2022-12-05 14:45:33', '2022-12-05 14:45:33', 10, 4, 'servermbkm', 'user denied: dev (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(888, NULL, '2022-12-05 14:45:33', '2022-12-05 14:45:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 27.105.67.54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15732]', NULL, NULL, NULL),
(889, NULL, '2022-12-05 15:51:15', '2022-12-05 15:51:15', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.151 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(890, NULL, '2022-12-05 15:51:56', '2022-12-05 15:51:56', 1, 5, '172.16.29.1', ' user jovial logged in from 202.138.225.151 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(891, NULL, '2022-12-05 15:52:06', '2022-12-05 15:52:06', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.151 via telnet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(892, NULL, '2022-12-05 15:53:10', '2022-12-05 15:53:10', 1, 5, '172.16.29.1', ' user jovial logged out from 202.138.225.151 via winbox', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info,account', NULL, NULL, NULL),
(893, NULL, '2022-12-05 22:58:56', '2022-12-05 22:58:56', 1, 5, 'info', ' file \"disk/RouterOS-2022dec05.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(894, NULL, '2022-12-06 03:01:43', '2022-12-06 03:01:43', 2, 6, 'servermbkm', '2B5K1h5Q048051: from=root, size=30568, class=0, nrcpts=1, msgid=<202212052001.2B5K1h5Q048051@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48051]', NULL, NULL, NULL),
(895, NULL, '2022-12-06 03:01:43', '2022-12-06 03:01:43', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48051]', NULL, NULL, NULL),
(896, NULL, '2022-12-06 03:01:43', '2022-12-06 03:01:43', 2, 6, 'servermbkm', '2B5K1h5Q048051: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=60568, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B5K1hLW048076 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48051]', NULL, NULL, NULL),
(897, NULL, '2022-12-06 03:01:47', '2022-12-06 03:01:47', 2, 6, 'servermbkm', '2B5K1lWe048138: from=root, size=2783, class=0, nrcpts=1, msgid=<202212052001.2B5K1lWe048138@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48138]', NULL, NULL, NULL),
(898, NULL, '2022-12-06 03:01:47', '2022-12-06 03:01:47', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48138]', NULL, NULL, NULL),
(899, NULL, '2022-12-06 03:01:47', '2022-12-06 03:01:47', 2, 6, 'servermbkm', '2B5K1lWe048138: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B5K1lwA048141 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[48138]', NULL, NULL, NULL),
(900, NULL, '2022-12-06 08:51:01', '2022-12-06 08:51:01', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(901, NULL, '2022-12-06 12:39:58', '2022-12-06 12:39:58', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>65.49.20.67[5358]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(902, NULL, '2022-12-06 22:58:53', '2022-12-06 22:58:53', 1, 5, 'info', ' file \"disk/RouterOS-2022dec06.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(903, NULL, '2022-12-06 23:34:24', '2022-12-06 23:34:24', 1, 5, '172.16.29.1', ' input: in:Upstream-LTN out:(unknown 0), src-mac d4:ca:6d:77:a9:bd, proto TCP (RST), 111.59.33.123:14472->103.126.10.236:22022, len 40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'firewall,info', NULL, NULL, NULL),
(904, NULL, '2022-12-07 03:01:43', '2022-12-07 03:01:43', 2, 6, 'servermbkm', '2B6K1h6W050607: from=root, size=41496, class=0, nrcpts=1, msgid=<202212062001.2B6K1h6W050607@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[50607]', NULL, NULL, NULL),
(905, NULL, '2022-12-07 03:01:43', '2022-12-07 03:01:43', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[50607]', NULL, NULL, NULL),
(906, NULL, '2022-12-07 03:01:43', '2022-12-07 03:01:43', 2, 6, 'servermbkm', '2B6K1h6W050607: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=71496, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B6K1hpQ050632 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[50607]', NULL, NULL, NULL),
(907, NULL, '2022-12-07 03:01:47', '2022-12-07 03:01:47', 2, 6, 'servermbkm', '2B6K1lFd050694: from=root, size=2783, class=0, nrcpts=1, msgid=<202212062001.2B6K1lFd050694@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[50694]', NULL, NULL, NULL),
(908, NULL, '2022-12-07 03:01:47', '2022-12-07 03:01:47', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[50694]', NULL, NULL, NULL),
(909, NULL, '2022-12-07 03:01:47', '2022-12-07 03:01:47', 2, 6, 'servermbkm', '2B6K1lFd050694: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B6K1lhs050697 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[50694]', NULL, NULL, NULL),
(910, NULL, '2022-12-07 03:02:27', '2022-12-07 03:02:27', 1, 5, '172.16.29.1', ' input: in:Upstream-LTN out:(unknown 0), src-mac d4:ca:6d:77:a9:bd, proto TCP (ACK,FIN), 111.59.33.123:58900->103.126.10.236:22022, len 52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'firewall,info', NULL, NULL, NULL),
(911, NULL, '2022-12-07 03:02:53', '2022-12-07 03:02:53', 1, 5, '172.16.29.1', ' input: in:Upstream-LTN out:(unknown 0), src-mac d4:ca:6d:77:a9:bd, proto TCP (ACK,FIN), 111.59.33.123:63101->103.126.10.236:22022, len 52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'firewall,info', NULL, NULL, NULL),
(912, NULL, '2022-12-07 03:03:44', '2022-12-07 03:03:44', 1, 5, '172.16.29.1', ' input: in:Upstream-LTN out:(unknown 0), src-mac d4:ca:6d:77:a9:bd, proto TCP (ACK,FIN), 111.59.33.123:18083->103.126.10.236:22022, len 52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'firewall,info', NULL, NULL, NULL),
(913, NULL, '2022-12-07 09:00:51', '2022-12-07 09:00:51', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(914, NULL, '2022-12-07 13:07:08', '2022-12-07 13:07:08', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>192.241.212.216[53260]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(915, NULL, '2022-12-07 19:24:40', '2022-12-07 19:24:40', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>184.105.247.244[12836]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(916, NULL, '2022-12-07 22:58:52', '2022-12-07 22:58:52', 1, 5, 'info', ' file \"disk/RouterOS-2022dec07.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(917, NULL, '2022-12-07 23:54:28', '2022-12-07 23:54:28', 1, 5, '172.16.29.1', ' sntp change time Dec/07/2022 23:55:48 => Dec/07/2022 23:55:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(918, NULL, '2022-12-08 00:09:28', '2022-12-08 00:09:28', 1, 5, '172.16.29.1', ' sntp change time Dec/08/2022 00:10:47 => Dec/08/2022 00:10:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'system,info', NULL, NULL, NULL),
(919, NULL, '2022-12-08 03:11:59', '2022-12-08 03:11:59', 2, 6, 'servermbkm', '2B7KBxfT053734: from=root, size=52450, class=0, nrcpts=1, msgid=<202212072011.2B7KBxfT053734@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53734]', NULL, NULL, NULL),
(920, NULL, '2022-12-08 03:11:59', '2022-12-08 03:11:59', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53734]', NULL, NULL, NULL),
(921, NULL, '2022-12-08 03:12:00', '2022-12-08 03:12:00', 2, 6, 'servermbkm', '2B7KBxfT053734: to=root, ctladdr=root (0/0), delay=00:00:01, xdelay=00:00:01, mailer=relay, pri=82450, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B7KBxZn053759 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53734]', NULL, NULL, NULL),
(922, NULL, '2022-12-08 03:12:04', '2022-12-08 03:12:04', 2, 6, 'servermbkm', '2B7KC4R7053821: from=root, size=2783, class=0, nrcpts=1, msgid=<202212072012.2B7KC4R7053821@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53821]', NULL, NULL, NULL),
(923, NULL, '2022-12-08 03:12:04', '2022-12-08 03:12:04', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53821]', NULL, NULL, NULL),
(924, NULL, '2022-12-08 03:12:04', '2022-12-08 03:12:04', 2, 6, 'servermbkm', '2B7KC4R7053821: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B7KC4Wk053824 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[53821]', NULL, NULL, NULL),
(925, NULL, '2022-12-08 07:49:04', '2022-12-08 07:49:04', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>216.218.206.82[17428]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(926, NULL, '2022-12-08 09:32:16', '2022-12-08 09:32:16', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(927, NULL, '2022-12-08 22:58:51', '2022-12-08 22:58:51', 1, 5, 'info', ' file \"disk/RouterOS-2022dec08.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(928, NULL, '2022-12-09 03:01:43', '2022-12-09 03:01:43', 2, 6, 'servermbkm', '2B8K1hlQ056336: from=root, size=68483, class=0, nrcpts=1, msgid=<202212082001.2B8K1hlQ056336@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[56336]', NULL, NULL, NULL),
(929, NULL, '2022-12-09 03:01:43', '2022-12-09 03:01:43', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[56336]', NULL, NULL, NULL),
(930, NULL, '2022-12-09 03:01:43', '2022-12-09 03:01:43', 2, 6, 'servermbkm', '2B8K1hlQ056336: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=98483, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B8K1hEl056361 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[56336]', NULL, NULL, NULL),
(931, NULL, '2022-12-09 03:01:47', '2022-12-09 03:01:47', 2, 6, 'servermbkm', '2B8K1l24056423: from=root, size=2783, class=0, nrcpts=1, msgid=<202212082001.2B8K1l24056423@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[56423]', NULL, NULL, NULL),
(932, NULL, '2022-12-09 03:01:47', '2022-12-09 03:01:47', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[56423]', NULL, NULL, NULL),
(933, NULL, '2022-12-09 03:01:48', '2022-12-09 03:01:48', 2, 6, 'servermbkm', '2B8K1l24056423: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B8K1l8D056426 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[56423]', NULL, NULL, NULL),
(934, NULL, '2022-12-09 06:40:45', '2022-12-09 06:40:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(935, NULL, '2022-12-09 06:40:46', '2022-12-09 06:40:46', 10, 4, 'servermbkm', 'user denied: database (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(936, NULL, '2022-12-09 06:40:47', '2022-12-09 06:40:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(937, NULL, '2022-12-09 06:40:47', '2022-12-09 06:40:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(938, NULL, '2022-12-09 06:40:48', '2022-12-09 06:40:48', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(939, NULL, '2022-12-09 06:40:49', '2022-12-09 06:40:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(940, NULL, '2022-12-09 06:40:49', '2022-12-09 06:40:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(941, NULL, '2022-12-09 06:40:50', '2022-12-09 06:40:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(942, NULL, '2022-12-09 06:40:50', '2022-12-09 06:40:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(943, NULL, '2022-12-09 06:40:51', '2022-12-09 06:40:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(944, NULL, '2022-12-09 06:40:52', '2022-12-09 06:40:52', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(945, NULL, '2022-12-09 06:40:52', '2022-12-09 06:40:52', 10, 4, 'servermbkm', 'user denied: ueer (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(946, NULL, '2022-12-09 06:40:53', '2022-12-09 06:40:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(947, NULL, '2022-12-09 06:40:54', '2022-12-09 06:40:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL);
INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(948, NULL, '2022-12-09 06:40:54', '2022-12-09 06:40:54', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(949, NULL, '2022-12-09 06:40:55', '2022-12-09 06:40:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(950, NULL, '2022-12-09 06:40:55', '2022-12-09 06:40:55', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(951, NULL, '2022-12-09 06:40:56', '2022-12-09 06:40:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(952, NULL, '2022-12-09 06:40:57', '2022-12-09 06:40:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[22504]', NULL, NULL, NULL),
(953, NULL, '2022-12-09 06:40:58', '2022-12-09 06:40:58', 10, 4, 'servermbkm', 'user denied: qnap (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(954, NULL, '2022-12-09 06:40:58', '2022-12-09 06:40:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(955, NULL, '2022-12-09 06:40:59', '2022-12-09 06:40:59', 10, 4, 'servermbkm', 'user denied: db (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(956, NULL, '2022-12-09 06:40:59', '2022-12-09 06:40:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(957, NULL, '2022-12-09 06:41:00', '2022-12-09 06:41:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(958, NULL, '2022-12-09 06:41:01', '2022-12-09 06:41:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(959, NULL, '2022-12-09 06:41:01', '2022-12-09 06:41:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(960, NULL, '2022-12-09 06:41:02', '2022-12-09 06:41:02', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(961, NULL, '2022-12-09 06:41:02', '2022-12-09 06:41:02', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(962, NULL, '2022-12-09 06:41:03', '2022-12-09 06:41:03', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(963, NULL, '2022-12-09 06:41:04', '2022-12-09 06:41:04', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(964, NULL, '2022-12-09 06:41:04', '2022-12-09 06:41:04', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(965, NULL, '2022-12-09 06:41:05', '2022-12-09 06:41:05', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(966, NULL, '2022-12-09 06:41:06', '2022-12-09 06:41:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(967, NULL, '2022-12-09 06:41:06', '2022-12-09 06:41:06', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(968, NULL, '2022-12-09 06:41:07', '2022-12-09 06:41:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(969, NULL, '2022-12-09 06:41:07', '2022-12-09 06:41:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(970, NULL, '2022-12-09 06:41:08', '2022-12-09 06:41:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(971, NULL, '2022-12-09 06:41:09', '2022-12-09 06:41:09', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(972, NULL, '2022-12-09 06:41:09', '2022-12-09 06:41:09', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(973, NULL, '2022-12-09 06:41:10', '2022-12-09 06:41:10', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(974, NULL, '2022-12-09 06:41:11', '2022-12-09 06:41:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(975, NULL, '2022-12-09 06:41:12', '2022-12-09 06:41:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(976, NULL, '2022-12-09 06:41:12', '2022-12-09 06:41:12', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(977, NULL, '2022-12-09 06:41:13', '2022-12-09 06:41:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(978, NULL, '2022-12-09 06:41:13', '2022-12-09 06:41:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(979, NULL, '2022-12-09 06:41:14', '2022-12-09 06:41:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(980, NULL, '2022-12-09 06:41:15', '2022-12-09 06:41:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(981, NULL, '2022-12-09 06:41:15', '2022-12-09 06:41:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(982, NULL, '2022-12-09 06:41:16', '2022-12-09 06:41:16', 10, 4, 'servermbkm', 'user denied: shop (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(983, NULL, '2022-12-09 06:41:17', '2022-12-09 06:41:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(984, NULL, '2022-12-09 06:41:17', '2022-12-09 06:41:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(985, NULL, '2022-12-09 06:41:18', '2022-12-09 06:41:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(986, NULL, '2022-12-09 06:41:18', '2022-12-09 06:41:18', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(987, NULL, '2022-12-09 06:41:19', '2022-12-09 06:41:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(988, NULL, '2022-12-09 06:41:20', '2022-12-09 06:41:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(989, NULL, '2022-12-09 06:41:20', '2022-12-09 06:41:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(990, NULL, '2022-12-09 06:41:21', '2022-12-09 06:41:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(991, NULL, '2022-12-09 06:41:21', '2022-12-09 06:41:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(992, NULL, '2022-12-09 06:41:22', '2022-12-09 06:41:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(993, NULL, '2022-12-09 06:41:23', '2022-12-09 06:41:23', 10, 4, 'servermbkm', 'user denied: shopdb (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(994, NULL, '2022-12-09 06:41:23', '2022-12-09 06:41:23', 10, 4, 'servermbkm', 'user denied: sql (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(995, NULL, '2022-12-09 06:41:24', '2022-12-09 06:41:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(996, NULL, '2022-12-09 06:41:25', '2022-12-09 06:41:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(997, NULL, '2022-12-09 06:41:25', '2022-12-09 06:41:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(998, NULL, '2022-12-09 06:41:26', '2022-12-09 06:41:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(999, NULL, '2022-12-09 06:41:26', '2022-12-09 06:41:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(1000, NULL, '2022-12-09 06:41:27', '2022-12-09 06:41:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(1001, NULL, '2022-12-09 06:41:28', '2022-12-09 06:41:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(1002, NULL, '2022-12-09 06:41:28', '2022-12-09 06:41:28', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[52305]', NULL, NULL, NULL),
(1003, NULL, '2022-12-09 06:41:29', '2022-12-09 06:41:29', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1004, NULL, '2022-12-09 06:41:30', '2022-12-09 06:41:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1005, NULL, '2022-12-09 06:41:30', '2022-12-09 06:41:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1006, NULL, '2022-12-09 06:41:31', '2022-12-09 06:41:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1007, NULL, '2022-12-09 06:41:32', '2022-12-09 06:41:32', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1008, NULL, '2022-12-09 06:41:32', '2022-12-09 06:41:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1009, NULL, '2022-12-09 06:41:33', '2022-12-09 06:41:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1010, NULL, '2022-12-09 06:41:33', '2022-12-09 06:41:33', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1011, NULL, '2022-12-09 06:41:34', '2022-12-09 06:41:34', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1012, NULL, '2022-12-09 06:41:35', '2022-12-09 06:41:35', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1013, NULL, '2022-12-09 06:41:35', '2022-12-09 06:41:35', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1014, NULL, '2022-12-09 06:41:36', '2022-12-09 06:41:36', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1015, NULL, '2022-12-09 06:41:37', '2022-12-09 06:41:37', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1016, NULL, '2022-12-09 06:41:37', '2022-12-09 06:41:37', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1017, NULL, '2022-12-09 06:41:38', '2022-12-09 06:41:38', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1018, NULL, '2022-12-09 06:41:39', '2022-12-09 06:41:39', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1019, NULL, '2022-12-09 06:41:39', '2022-12-09 06:41:39', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1020, NULL, '2022-12-09 06:41:40', '2022-12-09 06:41:40', 10, 4, 'servermbkm', 'user denied: asustor (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1021, NULL, '2022-12-09 06:41:40', '2022-12-09 06:41:40', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1022, NULL, '2022-12-09 06:41:41', '2022-12-09 06:41:41', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1023, NULL, '2022-12-09 06:41:42', '2022-12-09 06:41:42', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1024, NULL, '2022-12-09 06:41:42', '2022-12-09 06:41:42', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1025, NULL, '2022-12-09 06:41:43', '2022-12-09 06:41:43', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1026, NULL, '2022-12-09 06:41:44', '2022-12-09 06:41:44', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1027, NULL, '2022-12-09 06:41:44', '2022-12-09 06:41:44', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1028, NULL, '2022-12-09 06:41:45', '2022-12-09 06:41:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1029, NULL, '2022-12-09 06:41:45', '2022-12-09 06:41:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1030, NULL, '2022-12-09 06:41:46', '2022-12-09 06:41:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1031, NULL, '2022-12-09 06:41:47', '2022-12-09 06:41:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1032, NULL, '2022-12-09 06:41:47', '2022-12-09 06:41:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1033, NULL, '2022-12-09 06:41:48', '2022-12-09 06:41:48', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1034, NULL, '2022-12-09 06:41:49', '2022-12-09 06:41:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1035, NULL, '2022-12-09 06:41:49', '2022-12-09 06:41:49', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1036, NULL, '2022-12-09 06:41:50', '2022-12-09 06:41:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1037, NULL, '2022-12-09 06:41:51', '2022-12-09 06:41:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1038, NULL, '2022-12-09 06:41:51', '2022-12-09 06:41:51', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1039, NULL, '2022-12-09 06:41:52', '2022-12-09 06:41:52', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1040, NULL, '2022-12-09 06:41:52', '2022-12-09 06:41:52', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1041, NULL, '2022-12-09 06:41:53', '2022-12-09 06:41:53', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1042, NULL, '2022-12-09 06:41:54', '2022-12-09 06:41:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1043, NULL, '2022-12-09 06:41:54', '2022-12-09 06:41:54', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1044, NULL, '2022-12-09 06:41:55', '2022-12-09 06:41:55', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1045, NULL, '2022-12-09 06:41:56', '2022-12-09 06:41:56', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1046, NULL, '2022-12-09 06:41:56', '2022-12-09 06:41:56', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1047, NULL, '2022-12-09 06:41:57', '2022-12-09 06:41:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1048, NULL, '2022-12-09 06:41:57', '2022-12-09 06:41:57', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1049, NULL, '2022-12-09 06:41:58', '2022-12-09 06:41:58', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1050, NULL, '2022-12-09 06:41:59', '2022-12-09 06:41:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1051, NULL, '2022-12-09 06:41:59', '2022-12-09 06:41:59', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1052, NULL, '2022-12-09 06:42:00', '2022-12-09 06:42:00', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1053, NULL, '2022-12-09 06:42:01', '2022-12-09 06:42:01', 10, 4, 'servermbkm', 'user denied: web (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15727]', NULL, NULL, NULL),
(1054, NULL, '2022-12-09 06:42:01', '2022-12-09 06:42:01', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1055, NULL, '2022-12-09 06:42:02', '2022-12-09 06:42:02', 10, 4, 'servermbkm', 'user denied: admin (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1056, NULL, '2022-12-09 06:42:03', '2022-12-09 06:42:03', 10, 4, 'servermbkm', 'user denied: user (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1057, NULL, '2022-12-09 06:42:03', '2022-12-09 06:42:03', 10, 4, 'servermbkm', 'user denied: developer (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1058, NULL, '2022-12-09 06:42:04', '2022-12-09 06:42:04', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1059, NULL, '2022-12-09 06:42:05', '2022-12-09 06:42:05', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1060, NULL, '2022-12-09 06:42:05', '2022-12-09 06:42:05', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1061, NULL, '2022-12-09 06:42:06', '2022-12-09 06:42:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1062, NULL, '2022-12-09 06:42:06', '2022-12-09 06:42:06', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1063, NULL, '2022-12-09 06:42:07', '2022-12-09 06:42:07', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1064, NULL, '2022-12-09 06:42:08', '2022-12-09 06:42:08', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1065, NULL, '2022-12-09 06:42:08', '2022-12-09 06:42:08', 10, 4, 'servermbkm', 'user denied: dbs (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1066, NULL, '2022-12-09 06:42:09', '2022-12-09 06:42:09', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1067, NULL, '2022-12-09 06:42:09', '2022-12-09 06:42:09', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1068, NULL, '2022-12-09 06:42:10', '2022-12-09 06:42:10', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1069, NULL, '2022-12-09 06:42:11', '2022-12-09 06:42:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1070, NULL, '2022-12-09 06:42:11', '2022-12-09 06:42:11', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1071, NULL, '2022-12-09 06:42:12', '2022-12-09 06:42:12', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1072, NULL, '2022-12-09 06:42:13', '2022-12-09 06:42:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1073, NULL, '2022-12-09 06:42:13', '2022-12-09 06:42:13', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1074, NULL, '2022-12-09 06:42:14', '2022-12-09 06:42:14', 10, 4, 'servermbkm', 'user denied: wp (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1075, NULL, '2022-12-09 06:42:14', '2022-12-09 06:42:14', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1076, NULL, '2022-12-09 06:42:15', '2022-12-09 06:42:15', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1077, NULL, '2022-12-09 06:42:16', '2022-12-09 06:42:16', 10, 4, 'servermbkm', 'user denied: wordspress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1078, NULL, '2022-12-09 06:42:16', '2022-12-09 06:42:16', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1079, NULL, '2022-12-09 06:42:17', '2022-12-09 06:42:17', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1080, NULL, '2022-12-09 06:42:17', '2022-12-09 06:42:17', 10, 4, 'servermbkm', 'user denied: pma (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1081, NULL, '2022-12-09 06:42:18', '2022-12-09 06:42:18', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1082, NULL, '2022-12-09 06:42:19', '2022-12-09 06:42:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1083, NULL, '2022-12-09 06:42:19', '2022-12-09 06:42:19', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1084, NULL, '2022-12-09 06:42:20', '2022-12-09 06:42:20', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1085, NULL, '2022-12-09 06:42:20', '2022-12-09 06:42:20', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1086, NULL, '2022-12-09 06:42:21', '2022-12-09 06:42:21', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1087, NULL, '2022-12-09 06:42:22', '2022-12-09 06:42:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1088, NULL, '2022-12-09 06:42:22', '2022-12-09 06:42:22', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1089, NULL, '2022-12-09 06:42:23', '2022-12-09 06:42:23', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1090, NULL, '2022-12-09 06:42:24', '2022-12-09 06:42:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1091, NULL, '2022-12-09 06:42:24', '2022-12-09 06:42:24', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1092, NULL, '2022-12-09 06:42:25', '2022-12-09 06:42:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1093, NULL, '2022-12-09 06:42:25', '2022-12-09 06:42:25', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1094, NULL, '2022-12-09 06:42:26', '2022-12-09 06:42:26', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1095, NULL, '2022-12-09 06:42:27', '2022-12-09 06:42:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1096, NULL, '2022-12-09 06:42:27', '2022-12-09 06:42:27', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1097, NULL, '2022-12-09 06:42:28', '2022-12-09 06:42:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1098, NULL, '2022-12-09 06:42:28', '2022-12-09 06:42:28', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1099, NULL, '2022-12-09 06:42:29', '2022-12-09 06:42:29', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1100, NULL, '2022-12-09 06:42:30', '2022-12-09 06:42:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1101, NULL, '2022-12-09 06:42:30', '2022-12-09 06:42:30', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1102, NULL, '2022-12-09 06:42:31', '2022-12-09 06:42:31', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1103, NULL, '2022-12-09 06:42:32', '2022-12-09 06:42:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15728]', NULL, NULL, NULL),
(1104, NULL, '2022-12-09 06:42:32', '2022-12-09 06:42:32', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1105, NULL, '2022-12-09 06:42:33', '2022-12-09 06:42:33', 10, 4, 'servermbkm', 'user denied: root (empty-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1106, NULL, '2022-12-09 06:42:34', '2022-12-09 06:42:34', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1107, NULL, '2022-12-09 06:42:34', '2022-12-09 06:42:34', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1108, NULL, '2022-12-09 06:42:35', '2022-12-09 06:42:35', 10, 4, 'servermbkm', 'user denied: nas (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1109, NULL, '2022-12-09 06:42:36', '2022-12-09 06:42:36', 10, 4, 'servermbkm', 'user denied: dev (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1110, NULL, '2022-12-09 06:42:36', '2022-12-09 06:42:36', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1111, NULL, '2022-12-09 06:42:37', '2022-12-09 06:42:37', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1112, NULL, '2022-12-09 06:42:37', '2022-12-09 06:42:37', 10, 4, 'servermbkm', 'user denied: wordpress (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1113, NULL, '2022-12-09 06:42:38', '2022-12-09 06:42:38', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1114, NULL, '2022-12-09 06:42:39', '2022-12-09 06:42:39', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1115, NULL, '2022-12-09 06:42:39', '2022-12-09 06:42:39', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1116, NULL, '2022-12-09 06:42:40', '2022-12-09 06:42:40', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1117, NULL, '2022-12-09 06:42:41', '2022-12-09 06:42:41', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1118, NULL, '2022-12-09 06:42:41', '2022-12-09 06:42:41', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1119, NULL, '2022-12-09 06:42:42', '2022-12-09 06:42:42', 10, 4, 'servermbkm', 'user denied: blog (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1120, NULL, '2022-12-09 06:42:42', '2022-12-09 06:42:42', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1121, NULL, '2022-12-09 06:42:43', '2022-12-09 06:42:43', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1122, NULL, '2022-12-09 06:42:44', '2022-12-09 06:42:44', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1123, NULL, '2022-12-09 06:42:44', '2022-12-09 06:42:44', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1124, NULL, '2022-12-09 06:42:45', '2022-12-09 06:42:45', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1125, NULL, '2022-12-09 06:42:46', '2022-12-09 06:42:46', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1126, NULL, '2022-12-09 06:42:46', '2022-12-09 06:42:46', 10, 4, 'servermbkm', 'user denied: pma (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1127, NULL, '2022-12-09 06:42:47', '2022-12-09 06:42:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1128, NULL, '2022-12-09 06:42:47', '2022-12-09 06:42:47', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1129, NULL, '2022-12-09 06:42:48', '2022-12-09 06:42:48', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1130, NULL, '2022-12-09 06:42:49', '2022-12-09 06:42:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1131, NULL, '2022-12-09 06:42:49', '2022-12-09 06:42:49', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1132, NULL, '2022-12-09 06:42:50', '2022-12-09 06:42:50', 10, 4, 'servermbkm', 'user denied: root (mysql-denied) from 104.248.160.133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'phpMyAdmin[15729]', NULL, NULL, NULL),
(1133, NULL, '2022-12-09 08:02:38', '2022-12-09 08:02:38', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>184.105.139.102[36510]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1134, NULL, '2022-12-09 08:59:18', '2022-12-09 08:59:18', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(1135, NULL, '2022-12-09 13:08:20', '2022-12-09 13:08:20', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>198.199.108.87[34379]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1136, NULL, '2022-12-09 22:58:50', '2022-12-09 22:58:50', 1, 5, 'info', ' file \"disk/RouterOS-2022dec09.rsc\" uploaded', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'fetch:', NULL, NULL, NULL),
(1137, NULL, '2022-12-10 03:01:44', '2022-12-10 03:01:44', 2, 6, 'servermbkm', '2B9K1ir5064014: from=root, size=409674, class=0, nrcpts=1, msgid=<202212092001.2B9K1ir5064014@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64014]', NULL, NULL, NULL),
(1138, NULL, '2022-12-10 03:01:44', '2022-12-10 03:01:44', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64014]', NULL, NULL, NULL),
(1139, NULL, '2022-12-10 03:01:44', '2022-12-10 03:01:44', 2, 6, 'servermbkm', '2B9K1ir5064014: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=439674, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B9K1isQ064040 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64014]', NULL, NULL, NULL),
(1140, NULL, '2022-12-10 03:01:48', '2022-12-10 03:01:48', 2, 6, 'servermbkm', '2B9K1m4C064101: from=root, size=2783, class=0, nrcpts=1, msgid=<202212092001.2B9K1m4C064101@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64101]', NULL, NULL, NULL),
(1141, NULL, '2022-12-10 03:01:48', '2022-12-10 03:01:48', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64101]', NULL, NULL, NULL),
(1142, NULL, '2022-12-10 03:01:48', '2022-12-10 03:01:48', 2, 6, 'servermbkm', '2B9K1m4C064101: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=32783, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B9K1mQj064104 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64101]', NULL, NULL, NULL),
(1143, NULL, '2022-12-10 04:15:26', '2022-12-10 04:15:26', 2, 6, 'servermbkm', '2B9LFQjC064490: from=root, size=182, class=0, nrcpts=1, msgid=<202212092115.2B9LFQjC064490@servermbkm>, relay=root@localhost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64490]', NULL, NULL, NULL),
(1144, NULL, '2022-12-10 04:15:26', '2022-12-10 04:15:26', 2, 6, 'servermbkm', 'STARTTLS=client, relay=[127.0.0.1], version=TLSv1.3, verify=FAIL, cipher=TLS_AES_256_GCM_SHA384, bits=256/256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64490]', NULL, NULL, NULL),
(1145, NULL, '2022-12-10 04:15:26', '2022-12-10 04:15:26', 2, 6, 'servermbkm', '2B9LFQjC064490: to=root, ctladdr=root (0/0), delay=00:00:00, xdelay=00:00:00, mailer=relay, pri=30182, relay=[127.0.0.1] [127.0.0.1], dsn=2.0.0, stat=Sent (2B9LFQKK064493 Message accepted for delivery)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'sendmail[64490]', NULL, NULL, NULL),
(1146, NULL, '2022-12-10 09:12:20', '2022-12-10 09:12:20', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 146.88.240.4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(1147, NULL, '2022-12-10 12:17:46', '2022-12-10 12:17:46', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>128.14.226.202[56867]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL);
INSERT INTO `systemevents` (`ID`, `CustomerID`, `ReceivedAt`, `DeviceReportedTime`, `Facility`, `Priority`, `FromHost`, `Message`, `NTSeverity`, `Importance`, `EventSource`, `EventUser`, `EventCategory`, `EventID`, `EventBinaryData`, `MaxAvailable`, `CurrUsage`, `MinUsage`, `MaxUsage`, `InfoUnitID`, `SysLogTag`, `EventLogType`, `GenericFileName`, `SystemID`) VALUES
(1148, NULL, '2022-12-10 12:21:06', '2022-12-10 12:21:06', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>128.14.226.202[49696]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1149, NULL, '2022-12-10 12:22:20', '2022-12-10 12:22:20', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>128.14.226.202[37306]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1150, NULL, '2022-12-10 12:59:48', '2022-12-10 12:59:48', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>43.129.35.207[500]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1151, NULL, '2022-12-10 12:59:48', '2022-12-10 12:59:48', 1, 5, '172.16.29.1', ' the packet is retransmitted by 43.129.35.207[500].', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1152, NULL, '2022-12-10 13:59:45', '2022-12-10 13:59:45', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>65.49.20.69[28800]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1153, NULL, '2022-12-10 19:18:07', '2022-12-10 19:18:07', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 107.150.98.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(1154, NULL, '2022-12-10 19:36:25', '2022-12-10 19:36:25', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 107.150.98.68', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(1155, NULL, '2022-12-10 21:41:19', '2022-12-10 21:41:19', 1, 5, '172.16.29.1', ' respond new phase 1 (Identity Protection): 103.126.10.236[500]<=>103.47.135.144[34708]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1156, NULL, '2022-12-10 21:41:19', '2022-12-10 21:41:19', 1, 5, '172.16.29.1', ' ISAKMP-SA established 103.126.10.236[4500]-103.47.135.144[52794] spi:f80ad8f2133779af:a6ca4eb4234d72d3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'ipsec,info', NULL, NULL, NULL),
(1157, NULL, '2022-12-10 21:41:19', '2022-12-10 21:41:19', 1, 5, '172.16.29.1', ' first L2TP UDP packet received from 103.47.135.144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,info', NULL, NULL, NULL),
(1158, NULL, '2022-12-10 21:41:19', '2022-12-10 21:41:19', 1, 5, '172.16.29.1', ' shofari logged in, 172.16.28.6 from 103.47.135.144', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info,account', NULL, NULL, NULL),
(1159, NULL, '2022-12-10 21:41:20', '2022-12-10 21:41:20', 1, 5, '172.16.29.1', ' <l2tp-shofari>: authenticated', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL),
(1160, NULL, '2022-12-10 21:41:20', '2022-12-10 21:41:20', 1, 5, '172.16.29.1', ' <l2tp-shofari>: connected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'l2tp,ppp,info', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `systemeventsproperties`
--

CREATE TABLE `systemeventsproperties` (
  `ID` int(10) UNSIGNED NOT NULL,
  `SystemEventID` int(11) DEFAULT NULL,
  `ParamName` varchar(255) DEFAULT NULL,
  `ParamValue` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipes`
--

CREATE TABLE `tipes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tipes`
--

INSERT INTO `tipes` (`id`, `tipe`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Router', 2, '2022-12-25 22:13:34', '2022-12-25 22:13:34'),
(3, 'Switch', 1, '2022-12-26 17:00:11', '2022-12-26 17:00:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'user', 'user@ltn.net.id', '2022-12-23 01:12:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 1, '5GatcBTCPNmJYU2wMCL7OtY7MVEX4VvfJFWvcE2PHbAeGxAGqUBxbk5CKnkf', '2022-12-23 01:12:37', '2022-12-23 01:12:37'),
(2, 'admin', 'admin@ltn.net.id', '2022-12-23 01:12:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 2, 'rxqUriYJOpbDnofxkyvx4v2669CX2jTwzd3T8oUFLlDMKtmzvYDWb4HW2ImM', '2022-12-23 01:12:37', '2022-12-23 01:12:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_akses_perangkats`
--

CREATE TABLE `user_akses_perangkats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `perangkat_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brands_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `lokasis`
--
ALTER TABLE `lokasis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lokasis_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `perangkats`
--
ALTER TABLE `perangkats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perangkats_tipe_id_foreign` (`tipe_id`),
  ADD KEY `perangkats_brand_id_foreign` (`brand_id`),
  ADD KEY `perangkats_lokasi_id_foreign` (`lokasi_id`),
  ADD KEY `perangkats_add_by_foreign` (`add_by`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `systemevents`
--
ALTER TABLE `systemevents`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `systemeventsproperties`
--
ALTER TABLE `systemeventsproperties`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `tipes`
--
ALTER TABLE `tipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipes_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `user_akses_perangkats`
--
ALTER TABLE `user_akses_perangkats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_akses_perangkats_perangkat_id_foreign` (`perangkat_id`),
  ADD KEY `user_akses_perangkats_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `lokasis`
--
ALTER TABLE `lokasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `perangkats`
--
ALTER TABLE `perangkats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `systemevents`
--
ALTER TABLE `systemevents`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1161;

--
-- AUTO_INCREMENT untuk tabel `systemeventsproperties`
--
ALTER TABLE `systemeventsproperties`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tipes`
--
ALTER TABLE `tipes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_akses_perangkats`
--
ALTER TABLE `user_akses_perangkats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `lokasis`
--
ALTER TABLE `lokasis`
  ADD CONSTRAINT `lokasis_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `tipes`
--
ALTER TABLE `tipes`
  ADD CONSTRAINT `tipes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_akses_perangkats`
--
ALTER TABLE `user_akses_perangkats`
  ADD CONSTRAINT `user_akses_perangkats_perangkat_id_foreign` FOREIGN KEY (`perangkat_id`) REFERENCES `perangkats` (`id`),
  ADD CONSTRAINT `user_akses_perangkats_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
